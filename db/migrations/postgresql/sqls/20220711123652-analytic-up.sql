CREATE SCHEMA IF NOT EXISTS analytic;

CREATE TABLE IF NOT EXISTS analytic.pedestrians_locations_list (
	id serial4 NOT NULL,
	location_name_plain varchar(250) NULL,
	location_name varchar(250) NULL,
	lat numeric NULL,
	lng numeric NULL,
	address varchar(250) NULL,
	city_district varchar(250) NULL,
	tech varchar(250) NULL,
	map_picture varchar(2500) NULL,
	gate_picture varchar(2500) NULL,
	measurement_start timestamptz NULL,
	measurement_end timestamptz NULL,
	id_equiptment varchar(250) NULL,
	cube_id varchar(250) NULL,
	lon varbit NULL,
	map_image varchar(55) NULL,
	place_image varchar(59) NULL,
	"table" varchar(19) NULL,
	CONSTRAINT equipment_list_pkey PRIMARY KEY (id)
);
COMMENT ON TABLE analytic.pedestrians_locations_list IS '-- tabulka lokací. obsahuje lokace různých druhů a hodně custom dat, takže ji tvoříme ručně. sporadicky probíhají aktualizace
-- tabulka se nachází na https://operator.sharepoint.com/:f:/s/Datova_Platforma/EsE5uSdyLzdImWgyrMY8nFcBzwtH7GOr6TXtzNBn3FLIJw?e=fOov5s a jmenuje se locations_list.csv';

INSERT INTO analytic.pedestrians_locations_list (location_name_plain,location_name,lat,lng,address,city_district,tech,map_picture,gate_picture,measurement_start,measurement_end,id_equiptment,cube_id,lon,map_image,place_image,"table") VALUES
	 ('Výstaviště - viadukt VO 2','107043 - Výstaviště - viadukt VO 2',50.1042106575078,14.431679367811926,'U Výstaviště (viadukt)','Praha 7','kamera',NULL,NULL,'2020-09-02 00:00:00+02',NULL,NULL,'0',NULL,'https://storage.golemio.cz/intenzita-pesi-dopravy/1.png','https://storage.golemio.cz/intenzita-pesi-dopravy/1-map.png','flow_measurements'),
	 ('Rytířská 12','101336 - Rytířská 12',50.084781715549276,14.422412102465428,'Na Můstku 12','Praha 1','kamera',NULL,NULL,'2020-09-02 00:00:00+02',NULL,NULL,'1',NULL,'https://storage.golemio.cz/intenzita-pesi-dopravy/2.png','https://storage.golemio.cz/intenzita-pesi-dopravy/2-map.png','flow_measurements'),
	 ('Výstaviště - viadukt','Výstaviště - viadukt',50.1042106575078,14.431679367811926,'U Výstaviště (viadukt)','Praha 7','pyro',NULL,NULL,'2020-07-23 00:00:00+02','2020-11-10 00:00:00+01',NULL,'ecoCounter-100061215',NULL,'https://storage.golemio.cz/intenzita-pesi-dopravy/1.png','https://storage.golemio.cz/intenzita-pesi-dopravy/1-map.png','counters_detections'),
	 ('Stromovka - U Výstaviště','Stromovka - U Výstaviště',50.10489433551656,14.429609941952332,'U Výstaviště 67','Praha 7','wifi',NULL,NULL,'2020-11-24 00:00:00+01',NULL,NULL,'wifi1',NULL,NULL,'https://storage.golemio.cz/intenzita-pesi-dopravy/4-map.png','xyz'),
	 ('Stromovka - U Výstaviště','Stromovka - U Výstaviště',50.10489433551656,14.429609941952332,'U Výstaviště 67','Praha 7','pyro',NULL,NULL,'2020-11-11 00:00:00+01','2021-03-04 00:00:00+01',NULL,'ecoCounter-100065230',NULL,'https://storage.golemio.cz/intenzita-pesi-dopravy/3.png','https://storage.golemio.cz/intenzita-pesi-dopravy/3-map.png','counters_detections'),
	 ('Stromovka - U Výstaviště','Stromovka - U Výstaviště',50.10489433551656,14.429609941952332,'U Výstaviště 67','Praha 7','kamera',NULL,NULL,'2020-12-17 00:00:00+01',NULL,NULL,'13',NULL,'https://storage.golemio.cz/intenzita-pesi-dopravy/3.png','https://storage.golemio.cz/intenzita-pesi-dopravy/3-map.png','flow_measurements'),
	 ('Staroměstská věž pevná VI','101004 - Staroměstská věž pevná VI',50.08626,14.41301,'Karlův most','Praha 1','kamera',NULL,NULL,'2020-09-02 00:00:00+02',NULL,NULL,'9',NULL,'https://storage.golemio.cz/intenzita-pesi-dopravy/5.png','https://storage.golemio.cz/intenzita-pesi-dopravy/5-map.png','flow_measurements'),
	 ('Železniční most - nápl. I (náplavka)','202049 - Železniční most - nápl. I (náplavka)',50.06739,14.41468,'Novoměstská náplavka','Praha 2','kamera',NULL,NULL,'2020-09-02 00:00:00+02',NULL,NULL,'10',NULL,'https://storage.golemio.cz/intenzita-pesi-dopravy/6.png','https://storage.golemio.cz/intenzita-pesi-dopravy/6-map.png','flow_measurements'),
	 ('Železniční most - náplavka II','202050 - Železniční most - náplavka II',50.06693,14.41480,'Novoměstská náplavka','Praha 2','kamera',NULL,NULL,'2020-09-02 00:00:00+02',NULL,NULL,'11',NULL,'https://storage.golemio.cz/intenzita-pesi-dopravy/7.png','https://storage.golemio.cz/intenzita-pesi-dopravy/7-map.png','flow_measurements'),
	 ('Železniční most - nápl. I (rampa)','202049 - Železniční most - nápl. I (rampa)',50.06739,14.41468,'Novoměstská náplavka','Praha 2','kamera',NULL,NULL,'2020-09-02 00:00:00+02',NULL,NULL,'10',NULL,'https://storage.golemio.cz/intenzita-pesi-dopravy/6.png','https://storage.golemio.cz/intenzita-pesi-dopravy/6-map.png','flow_measurements')
ON CONFLICT DO NOTHING;
INSERT INTO analytic.pedestrians_locations_list (location_name_plain,location_name,lat,lng,address,city_district,tech,map_picture,gate_picture,measurement_start,measurement_end,id_equiptment,cube_id,lon,map_image,place_image,"table") VALUES
	 ('Železniční most - nápl. I (plný profil)','202049 - Železniční most - nápl. I (plný profil)',50.06739,14.41468,'Novoměstská náplavka','Praha 2','kamera',NULL,NULL,'2020-09-02 00:00:00+02',NULL,NULL,'10',NULL,'https://storage.golemio.cz/intenzita-pesi-dopravy/6.png','https://storage.golemio.cz/intenzita-pesi-dopravy/6-map.png','flow_measurements')
ON CONFLICT DO NOTHING;


CREATE TABLE IF NOT EXISTS analytic.pedestrians_locations_gates (
	direction_id varchar(250) NOT NULL,
	cube_id varchar(250) NOT NULL,
	location_id int4 NOT NULL,
	name varchar(250) NOT NULL,
	direction_type varchar(250) NOT NULL,
	CONSTRAINT equipment_gates_pkey PRIMARY KEY (direction_id, location_id)
);

INSERT INTO analytic.pedestrians_locations_gates (direction_id,cube_id,location_id,"name",direction_type) VALUES
	 ('4','0',1,'Směr pod viadukt','Směr 1'),
	 ('7','0',1,'Směr Výstaviště','Směr 2'),
	 ('2','1',2,'Směr Můstek','Směr 1'),
	 ('3','1',2,'Směr Rytířská','Směr 2'),
	 ('5','9',7,'Směr Malá strana','Směr 1'),
	 ('8','9',7,'Směr Staré město','Směr 2'),
	 ('6','10',8,'Náplavka, směr centrum','Směr 1'),
	 ('9','10',10,'Rampa, směr centrum','Směr 1'),
	 ('10','10',11,'Plný profil, směr centrum','Směr 1'),
	 ('11','10',8,'Náplavka, směr Vyšehrad','Směr 2')
ON CONFLICT DO NOTHING;
INSERT INTO analytic.pedestrians_locations_gates (direction_id,cube_id,location_id,"name",direction_type) VALUES
	 ('13','10',10,'Rampa, směr Vyšehrad','Směr 2'),
	 ('15','10',11,'Plný profil, směr Vyšehrad','Směr 2'),
	 ('35','11',9,'Plný profil, směr centrum','Směr 1'),
	 ('29','11',9,'Plný profil, směr Vyšehrad','Směr 2'),
	 ('7','13',6,'Směr Planetarium','Směr 1'),
	 ('8','13',6,'Směr Holešovice','Směr 2'),
	 ('ecoCounter-103061215','ecoCounter-100061215',3,'Směr pod viadukt','Směr 1'),
	 ('ecoCounter-104061215','ecoCounter-100061215',3,'Směr Výstaviště','Směr 2'),
	 ('ecoCounter-353245241','ecoCounter-100065230',5,'Směr Planetarium','Směr 1'),
	 ('ecoCounter-102065230','ecoCounter-100065230',5,'Směr Planetarium','Směr 1')
ON CONFLICT DO NOTHING;
INSERT INTO analytic.pedestrians_locations_gates (direction_id,cube_id,location_id,"name",direction_type) VALUES
	 ('ecoCounter-353240728','ecoCounter-100065230',5,'Směr Planetarium','Směr 1'),
	 ('ecoCounter-353235722','ecoCounter-100065230',5,'Směr Planetarium','Směr 1'),
	 ('ecoCounter-353231243','ecoCounter-100065230',5,'Směr Planetarium','Směr 1'),
	 ('ecoCounter-353245239','ecoCounter-100065230',5,'Směr Holešovice','Směr 2'),
	 ('ecoCounter-101065230','ecoCounter-100065230',5,'Směr Holešovice','Směr 2'),
	 ('ecoCounter-353240727','ecoCounter-100065230',5,'Směr Holešovice','Směr 2'),
	 ('ecoCounter-353235721','ecoCounter-100065230',5,'Směr Holešovice','Směr 2'),
	 ('ecoCounter-353231242','ecoCounter-100065230',5,'Směr Holešovice','Směr 2'),
	 ('dir1','wifi1',4,'Směr Holešovice','Směr 1'),
	 ('dir2','wifi1',4,'Směr Planetarium','Směr 2')
ON CONFLICT DO NOTHING;

COMMENT ON TABLE analytic.pedestrians_locations_gates IS '-- tabulka směrů k jednotlivým lokacím, platí to samé co u lokations_list.
-- nachází se na  https://operator.sharepoint.com/:f:/s/Datova_Platforma/EsE5uSdyLzdImWgyrMY8nFcBzwtH7GOr6TXtzNBn3FLIJw?e=fOov5s a jmenuje se directions_list.csv';

COMMENT ON TABLE pedestrians_wifi IS '-- tabulka na primární data wifi měření, jsou pouze jako csv na sharepointu https://operator.sharepoint.com/:f:/s/Datova_Platforma/EroMekiKaJdOtMPeF_Q8wPEBlZm5tLKHCgpK13H6hlc0Zg?e=4RbCkT';

-- analytic.v_pedestrians_detections_15min source

CREATE OR REPLACE VIEW analytic.v_pedestrians_detections_15min
AS WITH wifi AS (
         SELECT pedestrians_wifi.location_id AS locations_id,
            pedestrians_wifi.direction_id AS directions_id,
            date_trunc('hour'::text, pedestrians_wifi.measured_from) + (date_part('minute'::text, pedestrians_wifi.measured_from)::integer / 15)::double precision * '00:15:00'::interval AS measured_from,
            sum(pedestrians_wifi.value) AS value,
            count(pedestrians_wifi.value) AS count_n
           FROM flow.pedestrians_wifi
          GROUP BY pedestrians_wifi.location_id, pedestrians_wifi.direction_id, (date_trunc('hour'::text, pedestrians_wifi.measured_from) + (date_part('minute'::text, pedestrians_wifi.measured_from)::integer / 15)::double precision * '00:15:00'::interval)
        ), pyro AS (
         SELECT cd.locations_id,
            lg.first_dir AS directions_id,
            to_timestamp((cd.measured_from / 1000)::double precision) AS measured_from,
            sum(cd.value) AS value,
            count(cd.value) AS count_n
           FROM flow.counters_detections cd
             LEFT JOIN ( SELECT pedestrians_locations_gates.direction_id,
                    pedestrians_locations_gates.direction_type,
                    min(pedestrians_locations_gates.direction_id::text) OVER (PARTITION BY pedestrians_locations_gates.cube_id, pedestrians_locations_gates.direction_type) AS first_dir
                   FROM analytic.pedestrians_locations_gates) lg ON cd.directions_id::text = lg.direction_id::text
          WHERE cd.category::text = 'pedestrian'::text AND (cd.directions_id::text IN ( SELECT DISTINCT pedestrians_locations_gates.direction_id
                   FROM analytic.pedestrians_locations_gates))
          GROUP BY cd.locations_id, lg.first_dir, (to_timestamp((cd.measured_from / 1000)::double precision))
        ), flow AS (
         SELECT flow_measurements.cube_id::character varying(50) AS location_id,
            flow_measurements.sink_id::character varying(50) AS direction_id,
            date_trunc('hour'::text, to_timestamp((flow_measurements.start_timestamp / 1000)::double precision)) + (date_part('minute'::text, to_timestamp((flow_measurements.start_timestamp / 1000)::double precision))::integer / 15)::double precision * '00:15:00'::interval AS measured_from,
            sum(flow_measurements.value) AS value,
            count(flow_measurements.value) AS count_n
           FROM flow.flow_measurements
          WHERE ((flow_measurements.cube_id::character varying(50)::text, flow_measurements.sink_id::character varying(50)::text) IN ( SELECT DISTINCT pedestrians_locations_gates.cube_id AS location_id,
                    pedestrians_locations_gates.direction_id
                   FROM analytic.pedestrians_locations_gates)) AND flow_measurements.category::text = 'pedestrian'::text
          GROUP BY (flow_measurements.cube_id::character varying(50)), (flow_measurements.sink_id::character varying(50)), (date_trunc('hour'::text, to_timestamp((flow_measurements.start_timestamp / 1000)::double precision)) + (date_part('minute'::text, to_timestamp((flow_measurements.start_timestamp / 1000)::double precision))::integer / 15)::double precision * '00:15:00'::interval)
          ORDER BY (date_trunc('hour'::text, to_timestamp((flow_measurements.start_timestamp / 1000)::double precision)) + (date_part('minute'::text, to_timestamp((flow_measurements.start_timestamp / 1000)::double precision))::integer / 15)::double precision * '00:15:00'::interval), (flow_measurements.cube_id::character varying(50)), (flow_measurements.sink_id::character varying(50))
        ), measurements AS (
         SELECT wifi.locations_id,
            wifi.directions_id,
            wifi.measured_from,
            wifi.value,
            wifi.count_n
           FROM wifi
        UNION ALL
         SELECT pyro.locations_id,
            pyro.directions_id,
            pyro.measured_from,
            pyro.value,
            pyro.count_n
           FROM pyro
        UNION ALL
         SELECT flow.location_id,
            flow.direction_id,
            flow.measured_from,
            flow.value,
            flow.count_n
           FROM flow
        ), calendar AS (
         SELECT generate_series(min(measurements.measured_from), max(measurements.measured_from), '00:15:00'::interval) AS calendar
           FROM measurements
        ), classes AS (
         SELECT calendar.calendar,
            m_1.location_id,
            m_1.direction_id
           FROM calendar
             JOIN ( SELECT DISTINCT measurements.locations_id::character varying(50) AS location_id,
                    measurements.directions_id::character varying(50) AS direction_id
                   FROM measurements) m_1 ON true
        )
 SELECT c.calendar,
    date_trunc('hour'::text, c.calendar) AS calendar_hour,
    c.calendar::date AS calendar_date,
    ll.measurement_start,
    COALESCE(ll.measurement_end, now()::date::timestamp with time zone) AS measurement_end,
    c.location_id,
    c.direction_id,
    concat(c.location_id, '-', c.direction_id) AS compound_id,
    COALESCE(m.value, 0::bigint) AS value,
    COALESCE(m.count_n, 0::bigint) AS count_n
   FROM classes c
     LEFT JOIN measurements m ON c.calendar = m.measured_from AND c.location_id::text = m.locations_id::text AND c.direction_id::text = m.directions_id::text
     LEFT JOIN analytic.pedestrians_locations_list ll ON c.location_id::text = ll.cube_id::text
  WHERE c.calendar >= ll.measurement_start AND c.calendar <= COALESCE(ll.measurement_end, now()::date::timestamp with time zone);


-- analytic.v_pedestrians_detections_api source

CREATE OR REPLACE VIEW analytic.v_pedestrians_detections_api
AS WITH wifi AS (
         SELECT pedestrians_wifi.location_id,
            pedestrians_wifi.direction_id,
            date_trunc('hour'::text, pedestrians_wifi.measured_from) + (date_part('minute'::text, pedestrians_wifi.measured_from)::integer / 15)::double precision * '00:15:00'::interval AS measured_from,
            date_trunc('hour'::text, pedestrians_wifi.measured_from) + (date_part('minute'::text, pedestrians_wifi.measured_from)::integer / 15)::double precision * '00:15:00'::interval + '00:15:00'::interval AS measured_to,
            sum(pedestrians_wifi.value) AS value,
            count(pedestrians_wifi.value) AS count_n,
            3 AS quantity
           FROM flow.pedestrians_wifi
          GROUP BY pedestrians_wifi.location_id, pedestrians_wifi.direction_id, (date_trunc('hour'::text, pedestrians_wifi.measured_from) + (date_part('minute'::text, pedestrians_wifi.measured_from)::integer / 15)::double precision * '00:15:00'::interval), (date_trunc('hour'::text, pedestrians_wifi.measured_from) + (date_part('minute'::text, pedestrians_wifi.measured_from)::integer / 15)::double precision * '00:15:00'::interval + '00:15:00'::interval)
        ), pyro AS (
         SELECT cd.locations_id AS location_id,
            cd.directions_id AS direction_id,
            to_timestamp((cd.measured_from / 1000)::double precision) AS measured_from,
            to_timestamp((cd.measured_from / 1000)::double precision) + '00:15:00'::interval AS measured_to,
            sum(cd.value) AS value,
            count(cd.value) AS count_n,
            1 AS quantity
           FROM flow.counters_detections cd
          WHERE cd.category::text = 'pedestrian'::text AND (cd.directions_id::text IN ( SELECT DISTINCT pedestrians_directions_api.direction_id
                   FROM flow.pedestrians_directions_api))
          GROUP BY cd.locations_id, cd.directions_id, (to_timestamp((cd.measured_from / 1000)::double precision)), (to_timestamp((cd.measured_from / 1000)::double precision) + '00:15:00'::interval)
        ), flow AS (
         SELECT flow_measurements.cube_id::character varying(50) AS location_id,
            flow_measurements.sink_id::character varying(50) AS direction_id,
            date_trunc('hour'::text, to_timestamp((flow_measurements.start_timestamp / 1000)::double precision)) + (date_part('minute'::text, to_timestamp((flow_measurements.start_timestamp / 1000)::double precision))::integer / 15)::double precision * '00:15:00'::interval AS measured_from,
            date_trunc('hour'::text, to_timestamp((flow_measurements.start_timestamp / 1000)::double precision)) + (date_part('minute'::text, to_timestamp((flow_measurements.start_timestamp / 1000)::double precision))::integer / 15)::double precision * '00:15:00'::interval + '00:15:00'::interval AS measured_to,
            sum(flow_measurements.value) AS value,
            count(flow_measurements.value) AS count_n,
            3 AS quantity
           FROM flow.flow_measurements
          WHERE ((flow_measurements.cube_id::character varying(50)::text, flow_measurements.sink_id::character varying(50)::text) IN ( SELECT DISTINCT pedestrians_directions_api.cube_id AS location_id,
                    pedestrians_directions_api.direction_id
                   FROM flow.pedestrians_directions_api)) AND flow_measurements.category::text = 'pedestrian'::text
          GROUP BY (flow_measurements.cube_id::character varying(50)), (flow_measurements.sink_id::character varying(50)), (date_trunc('hour'::text, to_timestamp((flow_measurements.start_timestamp / 1000)::double precision)) + (date_part('minute'::text, to_timestamp((flow_measurements.start_timestamp / 1000)::double precision))::integer / 15)::double precision * '00:15:00'::interval), (date_trunc('hour'::text, to_timestamp((flow_measurements.start_timestamp / 1000)::double precision)) + (date_part('minute'::text, to_timestamp((flow_measurements.start_timestamp / 1000)::double precision))::integer / 15)::double precision * '00:15:00'::interval + '00:15:00'::interval)
          ORDER BY (date_trunc('hour'::text, to_timestamp((flow_measurements.start_timestamp / 1000)::double precision)) + (date_part('minute'::text, to_timestamp((flow_measurements.start_timestamp / 1000)::double precision))::integer / 15)::double precision * '00:15:00'::interval), (flow_measurements.cube_id::character varying(50)), (flow_measurements.sink_id::character varying(50))
        ), measurements AS (
         SELECT wifi.location_id,
            wifi.direction_id,
            wifi.measured_from,
            wifi.measured_to,
            wifi.value,
            wifi.count_n,
            wifi.quantity
           FROM wifi
        UNION ALL
         SELECT pyro.location_id,
            pyro.direction_id,
            pyro.measured_from,
            pyro.measured_to,
            pyro.value,
            pyro.count_n,
            pyro.quantity
           FROM pyro
        UNION ALL
         SELECT flow.location_id,
            flow.direction_id,
            flow.measured_from,
            flow.measured_to,
            flow.value,
            flow.count_n,
            flow.quantity
           FROM flow
        )
 SELECT measurements.measured_from,
    measurements.measured_to,
    pda.location_id::character varying(50) AS location_id,
    measurements.direction_id,
    measurements.value,
        CASE
            WHEN (measurements.count_n::numeric / measurements.quantity::numeric) > 1::numeric THEN 1::numeric
            ELSE measurements.count_n::numeric / measurements.quantity::numeric
        END AS quality
   FROM measurements
     LEFT JOIN flow.pedestrians_directions_api pda ON measurements.direction_id::text = pda.direction_id::text;


-- analytic.v_pedestrians_detections_daily source

CREATE OR REPLACE VIEW analytic.v_pedestrians_detections_daily
AS WITH wifi AS (
         SELECT pedestrians_wifi.location_id AS locations_id,
            pedestrians_wifi.direction_id AS directions_id,
            pedestrians_wifi.measured_from::date AS measured_from,
            sum(pedestrians_wifi.value) AS value,
            count(pedestrians_wifi.value) AS count_n,
            count(pedestrians_wifi.value)::numeric / 288::numeric AS quality_ratio
           FROM flow.pedestrians_wifi
          GROUP BY pedestrians_wifi.location_id, pedestrians_wifi.direction_id, (pedestrians_wifi.measured_from::date)
        ), pyro AS (
         SELECT cd.locations_id,
            lg.first_dir AS directions_id,
            to_timestamp((cd.measured_from / 1000)::double precision)::date AS measured_from,
            sum(cd.value) AS value,
            count(cd.value) / count(DISTINCT cd.directions_id) AS count_n,
            count(cd.value)::numeric / 96::numeric / count(DISTINCT cd.directions_id)::numeric AS quality_ratio
           FROM flow.counters_detections cd
             LEFT JOIN ( SELECT pedestrians_locations_gates.direction_id,
                    pedestrians_locations_gates.direction_type,
                    min(pedestrians_locations_gates.direction_id::text) OVER (PARTITION BY pedestrians_locations_gates.cube_id, pedestrians_locations_gates.direction_type) AS first_dir
                   FROM analytic.pedestrians_locations_gates) lg ON cd.directions_id::text = lg.direction_id::text
          WHERE cd.category::text = 'pedestrian'::text AND (cd.directions_id::text IN ( SELECT DISTINCT pedestrians_locations_gates.direction_id
                   FROM analytic.pedestrians_locations_gates))
          GROUP BY cd.locations_id, lg.first_dir, (to_timestamp((cd.measured_from / 1000)::double precision)::date)
        ), flow AS (
         SELECT flow_measurements.cube_id::character varying(50) AS location_id,
            flow_measurements.sink_id::character varying(50) AS direction_id,
            to_timestamp((flow_measurements.start_timestamp / 1000)::double precision)::date AS measured_from,
            sum(flow_measurements.value) AS value,
            count(flow_measurements.value) AS count_n,
            count(flow_measurements.value)::numeric / 288::numeric AS quality_ratio
           FROM flow.flow_measurements
          WHERE ((flow_measurements.cube_id::character varying(50)::text, flow_measurements.sink_id::character varying(50)::text) IN ( SELECT DISTINCT pedestrians_locations_gates.cube_id AS location_id,
                    pedestrians_locations_gates.direction_id
                   FROM analytic.pedestrians_locations_gates)) AND flow_measurements.category::text = 'pedestrian'::text
          GROUP BY (flow_measurements.cube_id::character varying(50)), (flow_measurements.sink_id::character varying(50)), (to_timestamp((flow_measurements.start_timestamp / 1000)::double precision)::date)
          ORDER BY (to_timestamp((flow_measurements.start_timestamp / 1000)::double precision)::date), (flow_measurements.cube_id::character varying(50)) DESC, (flow_measurements.sink_id::character varying(50))
        ), measurements AS (
         SELECT pyro.locations_id,
            pyro.directions_id,
            pyro.measured_from,
            pyro.value,
            pyro.count_n,
            pyro.quality_ratio
           FROM pyro
        UNION ALL
         SELECT flow.location_id,
            flow.direction_id,
            flow.measured_from,
            flow.value,
            flow.count_n,
            flow.quality_ratio
           FROM flow
        UNION ALL
         SELECT wifi.locations_id,
            wifi.directions_id,
            wifi.measured_from,
            wifi.value,
            wifi.count_n,
            wifi.quality_ratio
           FROM wifi
        ), calendar AS (
         SELECT generate_series(min(measurements.measured_from)::timestamp with time zone, max(measurements.measured_from)::timestamp with time zone, '1 day'::interval) AS calendar
           FROM measurements
        ), classes AS (
         SELECT calendar.calendar,
            m_1.location_id,
            m_1.direction_id
           FROM calendar
             JOIN ( SELECT DISTINCT measurements.locations_id::character varying(50) AS location_id,
                    measurements.directions_id::character varying(50) AS direction_id
                   FROM measurements) m_1 ON true
        )
 SELECT c.calendar,
    c.location_id,
    c.direction_id,
    concat(c.location_id, '-', c.direction_id) AS compound_id,
    COALESCE(m.value, 0::bigint) AS value,
    COALESCE(m.count_n, 0::bigint) AS count_n,
    COALESCE(m.quality_ratio, 0::numeric) AS quality_ratio,
        CASE
            WHEN c.calendar < ll.measurement_start OR c.calendar > ll.measurement_end THEN 4
            WHEN COALESCE(m.quality_ratio, 0::numeric) = 0::numeric THEN 3
            WHEN COALESCE(m.quality_ratio, 0::numeric) < 0.9 THEN 2
            WHEN COALESCE(m.quality_ratio, 0::numeric) > 0.9 AND COALESCE(m.quality_ratio, 0::numeric) < 1::numeric THEN 1
            WHEN COALESCE(m.quality_ratio, 0::numeric) >= 1::numeric THEN 0
            ELSE NULL::integer
        END AS quality_code,
        CASE
            WHEN c.calendar < ll.measurement_start OR c.calendar > ll.measurement_end THEN 'Nenainstalovaná technologie'::text
            WHEN COALESCE(m.quality_ratio, 0::numeric) = 0::numeric THEN 'Bez dat (0%)'::text
            WHEN COALESCE(m.quality_ratio, 0::numeric) < 0.9 THEN 'Částečná data (<90%)'::text
            WHEN COALESCE(m.quality_ratio, 0::numeric) > 0.9 AND COALESCE(m.quality_ratio, 0::numeric) < 1::numeric THEN 'Nekompletní data (>90%)'::text
            WHEN COALESCE(m.quality_ratio, 0::numeric) >= 1::numeric THEN 'Kompletní data (100%)'::text
            ELSE NULL::text
        END AS quality_status
   FROM classes c
     LEFT JOIN measurements m ON c.calendar = m.measured_from AND c.location_id::text = m.locations_id::text AND c.direction_id::text = m.directions_id
     LEFT JOIN analytic.pedestrians_locations_list ll ON c.location_id::text = ll.cube_id::text;


-- analytic.v_pedestrians_flow_quality source

CREATE OR REPLACE VIEW analytic.v_pedestrians_flow_quality
AS WITH timeline_category AS (
         SELECT DISTINCT fs.cube_id,
            fs.id AS sink_id,
            timeline."timestamp"
           FROM flow.flow_sinks fs
             LEFT JOIN ( SELECT generate_series(date_trunc('month'::text, CURRENT_DATE - '180 days'::interval)::timestamp with time zone, date_trunc('hour'::text, CURRENT_DATE::timestamp with time zone), '01:00:00'::interval) AS "timestamp") timeline ON true
        ), base_table AS (
         SELECT date_trunc('hour'::text, to_timestamp((flow_measurements.start_timestamp / 1000)::double precision)) AS start_timestamp,
            flow_measurements.cube_id,
            flow_measurements.sink_id,
            flow_measurements.category,
            count(flow_measurements.cube_id) AS count
           FROM flow.flow_measurements
          WHERE flow_measurements.start_timestamp::double precision >= (1000::double precision * date_part('epoch'::text, CURRENT_DATE - '210 days'::interval))
          GROUP BY (date_trunc('hour'::text, to_timestamp((flow_measurements.start_timestamp / 1000)::double precision))), flow_measurements.cube_id, flow_measurements.sink_id, flow_measurements.category
        ), real_nrow AS (
         SELECT base_table.start_timestamp,
            base_table.cube_id,
            base_table.sink_id,
            sum(base_table.count) AS n_row,
            sum(base_table.count) FILTER (WHERE base_table.category::text = 'pedestrian'::text) AS n_row_pedestrian
           FROM base_table
          GROUP BY base_table.start_timestamp, base_table.cube_id, base_table.sink_id
        ), expected_nrow AS (
         SELECT base_table.cube_id,
            base_table.sink_id,
            count(DISTINCT base_table.category) * 12 AS expected_nrow,
            12 AS expected_nrow_pedestrian
           FROM base_table
          WHERE base_table.start_timestamp >= (CURRENT_DATE - '30 days'::interval)
          GROUP BY base_table.cube_id, base_table.sink_id
        )
 SELECT t."timestamp",
    date_trunc('month'::text, t."timestamp")::date AS month,
    t.cube_id,
    t.sink_id,
    concat(t.cube_id, ' - ', t.sink_id) AS cube_sink,
    COALESCE(r.n_row, 0::bigint::numeric) AS n_row,
    COALESCE(r.n_row_pedestrian, 0::bigint::numeric) AS n_row_pedestrian,
    er.expected_nrow,
    er.expected_nrow_pedestrian
   FROM timeline_category t
     LEFT JOIN real_nrow r ON t."timestamp" = r.start_timestamp AND t.cube_id = r.cube_id AND t.sink_id = r.sink_id
     LEFT JOIN expected_nrow er ON t.cube_id = er.cube_id AND t.sink_id = er.sink_id;
