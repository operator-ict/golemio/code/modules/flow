-- update_detections_data procedure definition

CREATE OR replace procedure update_detections_data()
LANGUAGE plpgsql
AS $$
declare
	lastupdatetimestamp timestamptz;
	lastupdateunix bigint;
begin
	select
			case
				when flowmax.max_measured_from is not null
				then flowmax.max_measured_from
				else to_timestamp(0)
			end as max_measured_from into lastupdatetimestamp
	from (select max(measured_from) - interval '1 hours' as max_measured_from from flow.pedestrians_detections_api) flowMax;

	lastupdateunix := extract ('epoch' from lastupdatetimestamp) * 1000;


	insert into flow.pedestrians_detections_api
	with wifi as (
		select
			pedestrians_wifi.location_id,
			pedestrians_wifi.direction_id,
			date_trunc('hour'::text, pedestrians_wifi.measured_from) + (date_part('minute'::text, pedestrians_wifi.measured_from)::integer / 15)::double precision * '00:15:00'::interval as measured_from,
			date_trunc('hour'::text, pedestrians_wifi.measured_from) + (date_part('minute'::text, pedestrians_wifi.measured_from)::integer / 15)::double precision * '00:15:00'::interval + '00:15:00'::interval as measured_to,
			sum(pedestrians_wifi.value) as value,
			count(pedestrians_wifi.value) as count_n,
			3 as quantity
		from
			flow.pedestrians_wifi
		group by
			pedestrians_wifi.location_id,
			pedestrians_wifi.direction_id,
			(date_trunc('hour'::text, pedestrians_wifi.measured_from) + (date_part('minute'::text, pedestrians_wifi.measured_from)::integer / 15)::double precision * '00:15:00'::interval),
			(date_trunc('hour'::text, pedestrians_wifi.measured_from) + (date_part('minute'::text, pedestrians_wifi.measured_from)::integer / 15)::double precision * '00:15:00'::interval + '00:15:00'::interval)
	) SELECT wifi.measured_from,
	    wifi.measured_to,
	    wifi.location_id,
	    wifi.direction_id,
	    wifi.value,
	    wifi.count_n::numeric / wifi.quantity::numeric AS quality
	FROM wifi
	where wifi.measured_from > lastupdatetimestamp
	ON CONFLICT (location_id,direction_id,measured_from,measured_to)
		DO update
		SET value = EXCLUDED.value,
		quality = EXCLUDED.quality;

	insert into	flow.pedestrians_detections_api
	with pyro as (
		select
			cd.locations_id as location_id,
			cd.directions_id as direction_id,
			to_timestamp((cd.measured_from / 1000)::double precision) as measured_from,
			to_timestamp((cd.measured_from / 1000)::double precision) + '00:15:00'::interval as measured_to,
			sum(cd.value) as value,
			1 as count_n,
			1 as quantity
		from flow.counters_detections cd
		where
			cd.category::text = 'pedestrian'::text
			and (cd.directions_id::text in (select distinct pedestrians_directions_api.direction_id	from flow.pedestrians_directions_api))
		group by
			cd.locations_id,
			cd.directions_id,
			(to_timestamp((cd.measured_from / 1000)::double precision)),
			(to_timestamp((cd.measured_from / 1000)::double precision) + '00:15:00'::interval)
	)
	select
		pyro.measured_from,
		pyro.measured_to,
		pyro.location_id,
		pyro.direction_id,
		pyro.value,
		pyro.count_n::numeric / pyro.quantity::numeric as quality
	from pyro
	where pyro.measured_from > lastupdatetimestamp
	ON CONFLICT (location_id,direction_id,measured_from,measured_to)
		DO update
		SET value = EXCLUDED.value,
		quality = EXCLUDED.quality;


	insert into	flow.pedestrians_detections_api
    with vyber as (
        select * from flow.flow_measurements where flow_measurements.start_timestamp > lastupdateunix and category::text = 'pedestrian'::text
    ), flow as (
        select
            vyber.cube_id::character varying(50) as location_id,
            vyber.sink_id::character varying(50) as direction_id,
            date_trunc('hour'::text, to_timestamp((vyber.start_timestamp / 1000)::double precision)) + (date_part('minute'::text, to_timestamp((vyber.start_timestamp / 1000)::double precision))::integer / 15)::double precision * '00:15:00'::interval as measured_from,
            date_trunc('hour'::text, to_timestamp((vyber.start_timestamp / 1000)::double precision)) + (date_part('minute'::text, to_timestamp((vyber.start_timestamp / 1000)::double precision))::integer / 15)::double precision * '00:15:00'::interval + '00:15:00'::interval as measured_to,
            sum(vyber.value) as value,
            count(vyber.value) as count_n,
            3 as quantity
        from
            vyber
        where
            ((vyber.cube_id::character varying(50)::text,
            vyber.sink_id::character varying(50)::text) in (
            select distinct
                pedestrians_directions_api.cube_id as location_id,
                pedestrians_directions_api.direction_id
            from
                flow.pedestrians_directions_api))
        group by 1,2,3,4
	) select
		flow.measured_from,
		flow.measured_to,
		flow.location_id,
		flow.direction_id,
		flow.value,
		flow.count_n::numeric / flow.quantity::numeric as quality
	from flow
	where flow.measured_from > lastupdatetimestamp
	ON CONFLICT (location_id,direction_id,measured_from,measured_to)
		DO update
		SET value = EXCLUDED.value,
		quality = EXCLUDED.quality;
end;
$$;

-- ^^
