-- analytic.v_pedestrians_detections_api source

CREATE OR REPLACE VIEW analytic.v_pedestrians_detections_api
AS WITH wifi AS (
         SELECT pedestrians_wifi.location_id,
            pedestrians_wifi.direction_id,
            date_trunc('hour'::text, pedestrians_wifi.measured_from) + (date_part('minute'::text, pedestrians_wifi.measured_from)::integer / 15)::double precision * '00:15:00'::interval AS measured_from,
            date_trunc('hour'::text, pedestrians_wifi.measured_from) + (date_part('minute'::text, pedestrians_wifi.measured_from)::integer / 15)::double precision * '00:15:00'::interval + '00:15:00'::interval AS measured_to,
            sum(pedestrians_wifi.value) AS value,
            count(pedestrians_wifi.value) AS count_n,
            3 AS quantity
           FROM flow.pedestrians_wifi
          GROUP BY pedestrians_wifi.location_id, pedestrians_wifi.direction_id, (date_trunc('hour'::text, pedestrians_wifi.measured_from) + (date_part('minute'::text, pedestrians_wifi.measured_from)::integer / 15)::double precision * '00:15:00'::interval), (date_trunc('hour'::text, pedestrians_wifi.measured_from) + (date_part('minute'::text, pedestrians_wifi.measured_from)::integer / 15)::double precision * '00:15:00'::interval + '00:15:00'::interval)
        ), pyro AS (
         SELECT cd.locations_id AS location_id,
            cd.directions_id AS direction_id,
            cd.measured_from,
            cd.measured_from + '00:15:00'::interval AS measured_to,
            sum(cd.value) AS value,
            count(cd.value) AS count_n,
            1 AS quantity
           FROM flow.counters_detections cd
          WHERE cd.category::text = 'pedestrian'::text AND (cd.directions_id::text IN ( SELECT DISTINCT pedestrians_directions_api.direction_id
                   FROM flow.pedestrians_directions_api))
          GROUP BY cd.locations_id, cd.directions_id, cd.measured_from, (cd.measured_from + '00:15:00'::interval)
        ), flow AS (
         SELECT flow_measurements.cube_id::character varying(50) AS location_id,
            flow_measurements.sink_id::character varying(50) AS direction_id,
            date_trunc('hour'::text, flow_measurements.start_timestamp) + (date_part('minute'::text, flow_measurements.start_timestamp)::integer / 15)::double precision * '00:15:00'::interval AS measured_from,
            date_trunc('hour'::text, flow_measurements.start_timestamp) + (date_part('minute'::text, flow_measurements.start_timestamp)::integer / 15)::double precision * '00:15:00'::interval + '00:15:00'::interval AS measured_to,
            sum(flow_measurements.value) AS value,
            count(flow_measurements.value) AS count_n,
            3 AS quantity
           FROM flow.flow_measurements
          WHERE ((flow_measurements.cube_id::character varying(50)::text, flow_measurements.sink_id::character varying(50)::text) IN ( SELECT DISTINCT pedestrians_directions_api.cube_id AS location_id,
                    pedestrians_directions_api.direction_id
                   FROM flow.pedestrians_directions_api)) AND flow_measurements.category::text = 'pedestrian'::text
          GROUP BY (flow_measurements.cube_id::character varying(50)), (flow_measurements.sink_id::character varying(50)), (date_trunc('hour'::text, flow_measurements.start_timestamp) + (date_part('minute'::text, flow_measurements.start_timestamp)::integer / 15)::double precision * '00:15:00'::interval), (date_trunc('hour'::text, flow_measurements.start_timestamp) + (date_part('minute'::text, flow_measurements.start_timestamp)::integer / 15)::double precision * '00:15:00'::interval + '00:15:00'::interval)
          ORDER BY (date_trunc('hour'::text, flow_measurements.start_timestamp) + (date_part('minute'::text, flow_measurements.start_timestamp)::integer / 15)::double precision * '00:15:00'::interval), (flow_measurements.cube_id::character varying(50)), (flow_measurements.sink_id::character varying(50))
        ), measurements AS (
         SELECT wifi.location_id,
            wifi.direction_id,
            wifi.measured_from,
            wifi.measured_to,
            wifi.value,
            wifi.count_n,
            wifi.quantity
           FROM wifi
        UNION ALL
         SELECT pyro.location_id,
            pyro.direction_id,
            pyro.measured_from,
            pyro.measured_to,
            pyro.value,
            pyro.count_n,
            pyro.quantity
           FROM pyro
        UNION ALL
         SELECT flow.location_id,
            flow.direction_id,
            flow.measured_from,
            flow.measured_to,
            flow.value,
            flow.count_n,
            flow.quantity
           FROM flow
        )
 SELECT measurements.measured_from,
    measurements.measured_to,
    pda.location_id::character varying(50) AS location_id,
    measurements.direction_id,
    measurements.value,
        CASE
            WHEN (measurements.count_n::numeric / measurements.quantity::numeric) > 1::numeric THEN 1::numeric
            ELSE measurements.count_n::numeric / measurements.quantity::numeric
        END AS quality
   FROM measurements
     LEFT JOIN flow.pedestrians_directions_api pda ON measurements.direction_id::text = pda.direction_id::text AND measurements.location_id::text = pda.cube_id::text;
-- analytic.v_pedestrians_flow_quality source

CREATE OR REPLACE VIEW analytic.v_pedestrians_flow_quality
AS WITH timeline_category AS (
         SELECT DISTINCT fs.cube_id,
            fs.id AS sink_id,
            timeline."timestamp"
           FROM flow.flow_sinks fs
             LEFT JOIN ( SELECT generate_series(date_trunc('month'::text, CURRENT_DATE - '180 days'::interval)::timestamp with time zone, date_trunc('hour'::text, CURRENT_DATE::timestamp with time zone), '01:00:00'::interval) AS "timestamp") timeline ON true
        ), real_nrow AS (
         SELECT date_trunc('hour'::text, flow_measurements.start_timestamp) AS start_timestamp,
            flow_measurements.cube_id,
            flow_measurements.sink_id,
            count(*) AS n_row,
            count(*) FILTER (WHERE flow_measurements.category::text = 'pedestrian'::text) AS n_row_pedestrian
           FROM flow.flow_measurements
          WHERE EXTRACT(epoch FROM flow_measurements.start_timestamp)::double precision >= (1000::double precision * date_part('epoch'::text, CURRENT_DATE - '210 days'::interval))
          GROUP BY (date_trunc('hour'::text, flow_measurements.start_timestamp)), flow_measurements.cube_id, flow_measurements.sink_id
        )
 SELECT t."timestamp",
    date_trunc('month'::text, t."timestamp")::date AS month,
    t.cube_id,
    t.sink_id,
    concat(t.cube_id, ' - ', t.sink_id) AS cube_sink,
    COALESCE(r.n_row::numeric, 0::bigint::numeric) AS n_row,
    COALESCE(r.n_row_pedestrian::numeric, 0::bigint::numeric) AS n_row_pedestrian,
    108::bigint AS expected_nrow,
    12 AS expected_nrow_pedestrian
   FROM timeline_category t
     LEFT JOIN real_nrow r ON t."timestamp" = r.start_timestamp AND t.cube_id = r.cube_id AND t.sink_id = r.sink_id;

-- analytic.v_pedestrians_detections_daily source

CREATE OR REPLACE VIEW analytic.v_pedestrians_detections_daily
AS WITH wifi AS (
         SELECT pedestrians_wifi.location_id AS locations_id,
            pedestrians_wifi.direction_id AS directions_id,
            pedestrians_wifi.measured_from::date AS measured_from,
            sum(pedestrians_wifi.value) AS value,
            count(pedestrians_wifi.value) AS count_n,
            count(pedestrians_wifi.value)::numeric / 288::numeric AS quality_ratio
           FROM flow.pedestrians_wifi
          GROUP BY pedestrians_wifi.location_id, pedestrians_wifi.direction_id, (pedestrians_wifi.measured_from::date)
        ), pyro AS (
         SELECT cd.locations_id,
            lg.first_dir AS directions_id,
            cd.measured_from::date AS measured_from,
            sum(cd.value) AS value,
            count(cd.value) / count(DISTINCT cd.directions_id) AS count_n,
            count(cd.value)::numeric / 96::numeric / count(DISTINCT cd.directions_id)::numeric AS quality_ratio
           FROM flow.counters_detections cd
             LEFT JOIN ( SELECT pedestrians_locations_gates.direction_id,
                    pedestrians_locations_gates.direction_type,
                    min(pedestrians_locations_gates.direction_id::text) OVER (PARTITION BY pedestrians_locations_gates.cube_id, pedestrians_locations_gates.direction_type) AS first_dir
                   FROM analytic.pedestrians_locations_gates) lg ON cd.directions_id::text = lg.direction_id::text
          WHERE cd.category::text = 'pedestrian'::text AND (cd.directions_id::text IN ( SELECT DISTINCT pedestrians_locations_gates.direction_id
                   FROM analytic.pedestrians_locations_gates))
          GROUP BY cd.locations_id, lg.first_dir, (cd.measured_from::date)
        ), flow AS (
         SELECT flow_measurements.cube_id::character varying(50) AS location_id,
            flow_measurements.sink_id::character varying(50) AS direction_id,
            flow_measurements.start_timestamp::date AS measured_from,
            sum(flow_measurements.value) AS value,
            count(flow_measurements.value) AS count_n,
            count(flow_measurements.value)::numeric / 288::numeric AS quality_ratio
           FROM flow.flow_measurements
          WHERE ((flow_measurements.cube_id::character varying(50)::text, flow_measurements.sink_id::character varying(50)::text) IN ( SELECT DISTINCT pedestrians_locations_gates.cube_id AS location_id,
                    pedestrians_locations_gates.direction_id
                   FROM analytic.pedestrians_locations_gates)) AND flow_measurements.category::text = 'pedestrian'::text
          GROUP BY (flow_measurements.cube_id::character varying(50)), (flow_measurements.sink_id::character varying(50)), (flow_measurements.start_timestamp::date)
          ORDER BY (flow_measurements.start_timestamp::date), (flow_measurements.cube_id::character varying(50)) DESC, (flow_measurements.sink_id::character varying(50))
        ), measurements AS (
         SELECT pyro.locations_id,
            pyro.directions_id,
            pyro.measured_from,
            pyro.value,
            pyro.count_n,
            pyro.quality_ratio
           FROM pyro
        UNION ALL
         SELECT flow.location_id,
            flow.direction_id,
            flow.measured_from,
            flow.value,
            flow.count_n,
            flow.quality_ratio
           FROM flow
        UNION ALL
         SELECT wifi.locations_id,
            wifi.directions_id,
            wifi.measured_from,
            wifi.value,
            wifi.count_n,
            wifi.quality_ratio
           FROM wifi
        ), calendar AS (
         SELECT generate_series(min(measurements.measured_from)::timestamp with time zone, max(measurements.measured_from)::timestamp with time zone, '1 day'::interval) AS calendar
           FROM measurements
        ), classes AS (
         SELECT calendar.calendar,
            m_1.location_id,
            m_1.direction_id
           FROM calendar
             JOIN ( SELECT DISTINCT measurements.locations_id::character varying(50) AS location_id,
                    measurements.directions_id::character varying(50) AS direction_id
                   FROM measurements) m_1 ON true
        )
 SELECT c.calendar,
    c.location_id,
    c.direction_id,
    concat(c.location_id, '-', c.direction_id) AS compound_id,
    COALESCE(m.value, 0::bigint) AS value,
    COALESCE(m.count_n, 0::bigint) AS count_n,
    COALESCE(m.quality_ratio, 0::numeric) AS quality_ratio,
        CASE
            WHEN c.calendar < ll.measurement_start OR c.calendar > ll.measurement_end THEN 4
            WHEN COALESCE(m.quality_ratio, 0::numeric) = 0::numeric THEN 3
            WHEN COALESCE(m.quality_ratio, 0::numeric) < 0.9 THEN 2
            WHEN COALESCE(m.quality_ratio, 0::numeric) > 0.9 AND COALESCE(m.quality_ratio, 0::numeric) < 1::numeric THEN 1
            WHEN COALESCE(m.quality_ratio, 0::numeric) >= 1::numeric THEN 0
            ELSE NULL::integer
        END AS quality_code,
        CASE
            WHEN c.calendar < ll.measurement_start OR c.calendar > ll.measurement_end THEN 'Nenainstalovaná technologie'::text
            WHEN COALESCE(m.quality_ratio, 0::numeric) = 0::numeric THEN 'Bez dat (0%)'::text
            WHEN COALESCE(m.quality_ratio, 0::numeric) < 0.9 THEN 'Částečná data (<90%)'::text
            WHEN COALESCE(m.quality_ratio, 0::numeric) > 0.9 AND COALESCE(m.quality_ratio, 0::numeric) < 1::numeric THEN 'Nekompletní data (>90%)'::text
            WHEN COALESCE(m.quality_ratio, 0::numeric) >= 1::numeric THEN 'Kompletní data (100%)'::text
            ELSE NULL::text
        END AS quality_status
   FROM classes c
     LEFT JOIN measurements m ON c.calendar = m.measured_from AND c.location_id::text = m.locations_id::text AND c.direction_id::text = m.directions_id
     LEFT JOIN analytic.pedestrians_locations_list ll ON c.location_id::text = ll.cube_id::text;

-- analytic.v_pedestrians_detections_15min source

CREATE OR REPLACE VIEW analytic.v_pedestrians_detections_15min
AS WITH wifi AS (
         SELECT pedestrians_wifi.location_id AS locations_id,
            pedestrians_wifi.direction_id AS directions_id,
            date_trunc('hour'::text, pedestrians_wifi.measured_from) + (date_part('minute'::text, pedestrians_wifi.measured_from)::integer / 15)::double precision * '00:15:00'::interval AS measured_from,
            sum(pedestrians_wifi.value) AS value,
            count(pedestrians_wifi.value) AS count_n
           FROM flow.pedestrians_wifi
          GROUP BY pedestrians_wifi.location_id, pedestrians_wifi.direction_id, (date_trunc('hour'::text, pedestrians_wifi.measured_from) + (date_part('minute'::text, pedestrians_wifi.measured_from)::integer / 15)::double precision * '00:15:00'::interval)
        ), pyro AS (
         SELECT cd.locations_id,
            lg.first_dir AS directions_id,
            cd.measured_from,
            sum(cd.value) AS value,
            count(cd.value) AS count_n
           FROM flow.counters_detections cd
             LEFT JOIN ( SELECT pedestrians_locations_gates.direction_id,
                    pedestrians_locations_gates.direction_type,
                    min(pedestrians_locations_gates.direction_id::text) OVER (PARTITION BY pedestrians_locations_gates.cube_id, pedestrians_locations_gates.direction_type) AS first_dir
                   FROM analytic.pedestrians_locations_gates) lg ON cd.directions_id::text = lg.direction_id::text
          WHERE cd.category::text = 'pedestrian'::text AND (cd.directions_id::text IN ( SELECT DISTINCT pedestrians_locations_gates.direction_id
                   FROM analytic.pedestrians_locations_gates))
          GROUP BY cd.locations_id, lg.first_dir, cd.measured_from
        ), flow AS (
         SELECT flow_measurements.cube_id::character varying(50) AS location_id,
            flow_measurements.sink_id::character varying(50) AS direction_id,
            date_trunc('hour'::text, flow_measurements.start_timestamp) + (date_part('minute'::text, flow_measurements.start_timestamp)::integer / 15)::double precision * '00:15:00'::interval AS measured_from,
            sum(flow_measurements.value) AS value,
            count(flow_measurements.value) AS count_n
           FROM flow.flow_measurements
          WHERE ((flow_measurements.cube_id::character varying(50)::text, flow_measurements.sink_id::character varying(50)::text) IN ( SELECT DISTINCT pedestrians_locations_gates.cube_id AS location_id,
                    pedestrians_locations_gates.direction_id
                   FROM analytic.pedestrians_locations_gates)) AND flow_measurements.category::text = 'pedestrian'::text
          GROUP BY (flow_measurements.cube_id::character varying(50)), (flow_measurements.sink_id::character varying(50)), (date_trunc('hour'::text, flow_measurements.start_timestamp) + (date_part('minute'::text, flow_measurements.start_timestamp)::integer / 15)::double precision * '00:15:00'::interval)
          ORDER BY (date_trunc('hour'::text, flow_measurements.start_timestamp) + (date_part('minute'::text, flow_measurements.start_timestamp)::integer / 15)::double precision * '00:15:00'::interval), (flow_measurements.cube_id::character varying(50)), (flow_measurements.sink_id::character varying(50))
        ), measurements AS (
         SELECT wifi.locations_id,
            wifi.directions_id,
            wifi.measured_from,
            wifi.value,
            wifi.count_n
           FROM wifi
        UNION ALL
         SELECT pyro.locations_id,
            pyro.directions_id,
            pyro.measured_from,
            pyro.value,
            pyro.count_n
           FROM pyro
        UNION ALL
         SELECT flow.location_id,
            flow.direction_id,
            flow.measured_from,
            flow.value,
            flow.count_n
           FROM flow
        ), calendar AS (
         SELECT generate_series(min(measurements.measured_from), max(measurements.measured_from), '00:15:00'::interval) AS calendar
           FROM measurements
        ), classes AS (
         SELECT calendar.calendar,
            m_1.location_id,
            m_1.direction_id
           FROM calendar
             JOIN ( SELECT DISTINCT measurements.locations_id::character varying(50) AS location_id,
                    measurements.directions_id::character varying(50) AS direction_id
                   FROM measurements) m_1 ON true
        )
 SELECT c.calendar,
    date_trunc('hour'::text, c.calendar) AS calendar_hour,
    c.calendar::date AS calendar_date,
    ll.measurement_start,
    COALESCE(ll.measurement_end, now()::date::timestamp with time zone) AS measurement_end,
    c.location_id,
    c.direction_id,
    concat(c.location_id, '-', c.direction_id) AS compound_id,
    COALESCE(m.value, 0::bigint) AS value,
    COALESCE(m.count_n, 0::bigint) AS count_n
   FROM classes c
     LEFT JOIN measurements m ON c.calendar = m.measured_from AND c.location_id::text = m.locations_id::text AND c.direction_id::text = m.directions_id::text
     LEFT JOIN analytic.pedestrians_locations_list ll ON c.location_id::text = ll.cube_id::text
  WHERE c.calendar >= ll.measurement_start AND c.calendar <= COALESCE(ll.measurement_end, now()::date::timestamp with time zone);