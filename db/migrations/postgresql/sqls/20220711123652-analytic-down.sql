
DROP VIEW IF EXISTS analytic.v_pedestrians_detections_15min;
DROP VIEW IF EXISTS analytic.v_pedestrians_detections_api;
DROP VIEW IF EXISTS analytic.v_pedestrians_detections_daily;
DROP VIEW IF EXISTS analytic.v_pedestrians_flow_quality;
drop table if exists analytic.pedestrians_locations_gates;
drop table if exists analytic.pedestrians_locations_list;
