CREATE TABLE flow_analytics (
	id int4 NOT NULL,
	"name" varchar(150) NULL,
	sequence_number int4 NOT NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT flow_analyticspkey PRIMARY KEY (id, sequence_number)
);

CREATE TABLE flow_cubes (
	id int4 NOT NULL,
	"name" varchar(150) NOT NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT flow_cubespkey PRIMARY KEY (id, name)
);

CREATE TABLE flow_measurements (
	sink_id int4 NOT NULL,
	cube_id int4 NOT NULL,
	sequence_number int4 NOT NULL,
	analytic_id int4 NOT NULL,
	start_timestamp int8 NOT NULL,
	end_timestamp int8 NOT NULL,
	category varchar(150) NOT NULL,
	value int4 NOT NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	data_validity varchar(50) NULL,
	CONSTRAINT flow_measurementspkey PRIMARY KEY (cube_id, sink_id, start_timestamp, end_timestamp, category, sequence_number)
)
PARTITION BY RANGE (start_timestamp);


CREATE INDEX flow_measurements_ceated_at ON flow_measurements USING btree (created_at);
CREATE INDEX flow_measurements_start_cube_sink ON flow_measurements USING btree (cube_id, sink_id);
CREATE INDEX flow_measurements_start_timestamp ON flow_measurements USING btree (start_timestamp);

-- partitions
CREATE TABLE flow_measurements_min PARTITION OF flow_measurements FOR VALUES FROM (MINVALUE) TO (extract ('epoch' from '2020-09-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2020m09 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2020-09-01'::timestamp)*1000) TO (extract ('epoch' from '2020-10-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2020m10 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2020-10-01'::timestamp)*1000) TO (extract ('epoch' from '2020-11-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2020m11 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2020-11-01'::timestamp)*1000) TO (extract ('epoch' from '2020-12-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2020m12 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2020-12-01'::timestamp)*1000) TO (extract ('epoch' from '2021-01-01'::timestamp)*1000);
--2021
CREATE TABLE flow_measurements_y2021m01 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2021-01-01'::timestamp)*1000) TO (extract ('epoch' from '2021-02-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2021m02 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2021-02-01'::timestamp)*1000) TO (extract ('epoch' from '2021-03-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2021m03 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2021-03-01'::timestamp)*1000) TO (extract ('epoch' from '2021-04-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2021m04 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2021-04-01'::timestamp)*1000) TO (extract ('epoch' from '2021-05-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2021m05 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2021-05-01'::timestamp)*1000) TO (extract ('epoch' from '2021-06-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2021m06 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2021-06-01'::timestamp)*1000) TO (extract ('epoch' from '2021-07-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2021m07 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2021-07-01'::timestamp)*1000) TO (extract ('epoch' from '2021-08-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2021m08 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2021-08-01'::timestamp)*1000) TO (extract ('epoch' from '2021-09-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2021m09 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2021-09-01'::timestamp)*1000) TO (extract ('epoch' from '2021-10-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2021m10 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2021-10-01'::timestamp)*1000) TO (extract ('epoch' from '2021-11-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2021m11 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2021-11-01'::timestamp)*1000) TO (extract ('epoch' from '2021-12-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2021m12 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2021-12-01'::timestamp)*1000) TO (extract ('epoch' from '2022-01-01'::timestamp)*1000);
--2022
CREATE TABLE flow_measurements_y2022m01 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2022-01-01'::timestamp)*1000) TO (extract ('epoch' from '2022-02-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2022m02 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2022-02-01'::timestamp)*1000) TO (extract ('epoch' from '2022-03-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2022m03 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2022-03-01'::timestamp)*1000) TO (extract ('epoch' from '2022-04-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2022m04 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2022-04-01'::timestamp)*1000) TO (extract ('epoch' from '2022-05-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2022m05 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2022-05-01'::timestamp)*1000) TO (extract ('epoch' from '2022-06-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2022m06 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2022-06-01'::timestamp)*1000) TO (extract ('epoch' from '2022-07-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2022m07 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2022-07-01'::timestamp)*1000) TO (extract ('epoch' from '2022-08-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2022m08 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2022-08-01'::timestamp)*1000) TO (extract ('epoch' from '2022-09-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2022m09 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2022-09-01'::timestamp)*1000) TO (extract ('epoch' from '2022-10-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2022m10 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2022-10-01'::timestamp)*1000) TO (extract ('epoch' from '2022-11-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2022m11 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2022-11-01'::timestamp)*1000) TO (extract ('epoch' from '2022-12-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2022m12 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2022-12-01'::timestamp)*1000) TO (extract ('epoch' from '2023-01-01'::timestamp)*1000);
--2023
CREATE TABLE flow_measurements_y2023m01 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2023-01-01'::timestamp)*1000) TO (extract ('epoch' from '2023-02-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2023m02 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2023-02-01'::timestamp)*1000) TO (extract ('epoch' from '2023-03-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2023m03 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2023-03-01'::timestamp)*1000) TO (extract ('epoch' from '2023-04-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2023m04 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2023-04-01'::timestamp)*1000) TO (extract ('epoch' from '2023-05-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2023m05 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2023-05-01'::timestamp)*1000) TO (extract ('epoch' from '2023-06-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2023m06 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2023-06-01'::timestamp)*1000) TO (extract ('epoch' from '2023-07-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2023m07 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2023-07-01'::timestamp)*1000) TO (extract ('epoch' from '2023-08-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2023m08 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2023-08-01'::timestamp)*1000) TO (extract ('epoch' from '2023-09-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2023m09 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2023-09-01'::timestamp)*1000) TO (extract ('epoch' from '2023-10-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2023m10 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2023-10-01'::timestamp)*1000) TO (extract ('epoch' from '2023-11-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2023m11 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2023-11-01'::timestamp)*1000) TO (extract ('epoch' from '2023-12-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2023m12 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2023-12-01'::timestamp)*1000) TO (extract ('epoch' from '2024-01-01'::timestamp)*1000);
--2024
CREATE TABLE flow_measurements_y2024m01 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2024-01-01'::timestamp)*1000) TO (extract ('epoch' from '2024-02-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2024m02 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2024-02-01'::timestamp)*1000) TO (extract ('epoch' from '2024-03-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2024m03 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2024-03-01'::timestamp)*1000) TO (extract ('epoch' from '2024-04-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2024m04 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2024-04-01'::timestamp)*1000) TO (extract ('epoch' from '2024-05-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2024m05 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2024-05-01'::timestamp)*1000) TO (extract ('epoch' from '2024-06-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2024m06 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2024-06-01'::timestamp)*1000) TO (extract ('epoch' from '2024-07-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2024m07 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2024-07-01'::timestamp)*1000) TO (extract ('epoch' from '2024-08-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2024m08 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2024-08-01'::timestamp)*1000) TO (extract ('epoch' from '2024-09-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2024m09 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2024-09-01'::timestamp)*1000) TO (extract ('epoch' from '2024-10-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2024m10 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2024-10-01'::timestamp)*1000) TO (extract ('epoch' from '2024-11-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2024m11 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2024-11-01'::timestamp)*1000) TO (extract ('epoch' from '2024-12-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2024m12 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2024-12-01'::timestamp)*1000) TO (extract ('epoch' from '2025-01-01'::timestamp)*1000);
--2025
CREATE TABLE flow_measurements_y2025m01 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2025-01-01'::timestamp)*1000) TO (extract ('epoch' from '2025-02-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2025m02 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2025-02-01'::timestamp)*1000) TO (extract ('epoch' from '2025-03-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2025m03 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2025-03-01'::timestamp)*1000) TO (extract ('epoch' from '2025-04-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2025m04 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2025-04-01'::timestamp)*1000) TO (extract ('epoch' from '2025-05-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2025m05 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2025-05-01'::timestamp)*1000) TO (extract ('epoch' from '2025-06-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2025m06 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2025-06-01'::timestamp)*1000) TO (extract ('epoch' from '2025-07-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2025m07 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2025-07-01'::timestamp)*1000) TO (extract ('epoch' from '2025-08-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2025m08 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2025-08-01'::timestamp)*1000) TO (extract ('epoch' from '2025-09-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2025m09 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2025-09-01'::timestamp)*1000) TO (extract ('epoch' from '2025-10-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2025m10 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2025-10-01'::timestamp)*1000) TO (extract ('epoch' from '2025-11-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2025m11 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2025-11-01'::timestamp)*1000) TO (extract ('epoch' from '2025-12-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2025m12 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2025-12-01'::timestamp)*1000) TO (extract ('epoch' from '2026-01-01'::timestamp)*1000);
--2026
CREATE TABLE flow_measurements_y2026m01 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2026-01-01'::timestamp)*1000) TO (extract ('epoch' from '2026-02-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2026m02 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2026-02-01'::timestamp)*1000) TO (extract ('epoch' from '2026-03-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2026m03 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2026-03-01'::timestamp)*1000) TO (extract ('epoch' from '2026-04-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2026m04 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2026-04-01'::timestamp)*1000) TO (extract ('epoch' from '2026-05-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2026m05 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2026-05-01'::timestamp)*1000) TO (extract ('epoch' from '2026-06-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2026m06 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2026-06-01'::timestamp)*1000) TO (extract ('epoch' from '2026-07-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2026m07 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2026-07-01'::timestamp)*1000) TO (extract ('epoch' from '2026-08-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2026m08 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2026-08-01'::timestamp)*1000) TO (extract ('epoch' from '2026-09-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2026m09 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2026-09-01'::timestamp)*1000) TO (extract ('epoch' from '2026-10-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2026m10 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2026-10-01'::timestamp)*1000) TO (extract ('epoch' from '2026-11-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2026m11 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2026-11-01'::timestamp)*1000) TO (extract ('epoch' from '2026-12-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2026m12 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2026-12-01'::timestamp)*1000) TO (extract ('epoch' from '2027-01-01'::timestamp)*1000);
--202
CREATE TABLE flow_measurements_y2027up PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2027-01-01'::timestamp)*1000) TO (maxvalue);


CREATE TABLE flow_od_measurements (
	cube_id int4 NOT NULL,
	analytic_id int4 NOT NULL,
	sequence_number int4 NOT NULL,
	sink_id int4 NOT NULL,
	start_timestamp int8 NOT NULL,
	end_timestamp int8 NOT NULL,
	category varchar(150) NOT NULL,
	origin varchar(150) NOT NULL,
	destination varchar(150) NOT NULL,
	value int4 NOT NULL,
	data_validity varchar(50) NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT flow_od_measurementspkey PRIMARY KEY (cube_id, sink_id, start_timestamp, end_timestamp, category, origin, destination, sequence_number)
);
CREATE INDEX flow_od_measurements_created_at ON flow_od_measurements USING btree (created_at);

CREATE TABLE flow_sinks (
	id int4 NOT NULL,
	"name" varchar(150) NULL,
	history_start_timestamp int8 NOT NULL,
	output_value_type varchar(150) NOT NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
    cube_id int4 NOT NULL,
	CONSTRAINT flow_sinkspkey PRIMARY KEY (id, cube_id, output_value_type)
);


-- Číselníky

CREATE TABLE pedestrians_directions_api (
	direction_id varchar(250) NOT NULL,
	cube_id varchar(250) NOT NULL,
	location_id int4 NOT NULL,
	direction_name varchar(50) NOT NULL,
	direction_type varchar(250) NOT NULL
);

INSERT INTO pedestrians_directions_api (direction_id,cube_id,location_id,direction_name,direction_type) VALUES
	 ('4','0',1,'Viadukt','1'),
	 ('7','0',1,'Výstaviště','2'),
	 ('2','1',2,'Můstek','1'),
	 ('3','1',2,'Rytířská','2'),
	 ('ecoCounter-103061215','ecoCounter-100061215',3,'Pod viadukt','1'),
	 ('ecoCounter-104061215','ecoCounter-100061215',3,'Výstaviště','2'),
	 ('dir1','wifi1',4,'Holešovice','1'),
	 ('dir2','wifi1',4,'Planetarium','2'),
	 ('ecoCounter-353245241','ecoCounter-100065230',5,'Planetarium','1'),
	 ('ecoCounter-101065230','ecoCounter-100065230',5,'Holešovice','2'),
	 ('7','13',6,'Planetarium','1'),
	 ('8','13',6,'Holešovice','2'),
	 ('5','9',7,'Malá strana','1'),
	 ('8','9',7,'Staré město','2'),
	 ('6','10',8,'Náplavka, směr centrum','1'),
	 ('11','10',8,'Náplavka, směr Vyšehrad','2'),
	 ('35','11',9,'Plný profil, směr centrum','1'),
	 ('29','11',9,'Plný profil, směr Vyšehrad','2'),
	 ('9','10',8,'Rampa, směr centrum','1'),
	 ('13','10',8,'Rampa, směr Vyšehrad','2'),
	 ('10','10',8,'Plný profil, směr centrum','1'),
	 ('15','10',8,'Plný profil, směr Vyšehrad','2');

CREATE TABLE pedestrians_locations_api (
	location_id serial4 NOT NULL,
	location_name varchar(250) NULL,
	lat numeric NULL,
	lng numeric NULL,
	address varchar(250) NULL,
	city_district varchar(250) NULL,
	tech varchar(250) NULL,
	map_url varchar(2500) NULL,
	place_url varchar(2500) NULL,
	measurement_start timestamptz NULL,
	measurement_end timestamptz NULL,
	cube_id varchar(250) NULL
);

INSERT INTO pedestrians_locations_api (location_name,lat,lng,address,city_district,tech,map_url,place_url,measurement_start,measurement_end,cube_id) VALUES
	 ('Výstaviště - viadukt VO 2',50.1042106575078,14.431679367811926,'U Výstaviště (viadukt)','Praha 7','kamera','https://storage.golemio.cz/intenzita-pesi-dopravy/1.png','https://storage.golemio.cz/intenzita-pesi-dopravy/1-map.png','2020-09-02 00:00:00+02',NULL,'0'),
	 ('Rytířská 12',50.084781715549276,14.422412102465428,'Na Můstku 12','Praha 1','kamera','https://storage.golemio.cz/intenzita-pesi-dopravy/2.png','https://storage.golemio.cz/intenzita-pesi-dopravy/2-map.png','2020-09-02 00:00:00+02',NULL,'1'),
	 ('Výstaviště - viadukt',50.1042106575078,14.431679367811926,'U Výstaviště (viadukt)','Praha 7','pyro','https://storage.golemio.cz/intenzita-pesi-dopravy/1.png','https://storage.golemio.cz/intenzita-pesi-dopravy/1-map.png','2020-07-23 00:00:00+02','2020-11-10 00:00:00+01','ecoCounter-100061215'),
	 ('Stromovka - U Výstaviště',50.10489433551656,14.429609941952332,'U Výstaviště 67','Praha 7','wifi',NULL,'https://storage.golemio.cz/intenzita-pesi-dopravy/4-map.png','2020-11-24 00:00:00+01',NULL,'wifi1'),
	 ('Stromovka - U Výstaviště',50.10489433551656,14.429609941952332,'U Výstaviště 67','Praha 7','pyro','https://storage.golemio.cz/intenzita-pesi-dopravy/3.png','https://storage.golemio.cz/intenzita-pesi-dopravy/3-map.png','2020-11-11 00:00:00+01','2021-03-04 00:00:00+01','ecoCounter-100065230'),
	 ('Stromovka - U Výstaviště',50.10489433551656,14.429609941952332,'U Výstaviště 67','Praha 7','kamera','https://storage.golemio.cz/intenzita-pesi-dopravy/3.png','https://storage.golemio.cz/intenzita-pesi-dopravy/3-map.png','2020-12-17 00:00:00+01',NULL,'13'),
	 ('Staroměstská věž pevná VI',50.08626,14.41301,'Karlův most','Praha 1','kamera','https://storage.golemio.cz/intenzita-pesi-dopravy/5.png','https://storage.golemio.cz/intenzita-pesi-dopravy/5-map.png','2020-09-02 00:00:00+02',NULL,'9'),
	 ('Železniční most - náplavka I',50.06739,14.41468,'Novoměstská náplavka','Praha 2','kamera','https://storage.golemio.cz/intenzita-pesi-dopravy/6.png','https://storage.golemio.cz/intenzita-pesi-dopravy/6-map.png','2020-09-02 00:00:00+02',NULL,'10'),
	 ('Železniční most - náplavka II',50.06693,14.41480,'Novoměstská náplavka','Praha 2','kamera','https://storage.golemio.cz/intenzita-pesi-dopravy/7.png','https://storage.golemio.cz/intenzita-pesi-dopravy/7-map.png','2020-09-02 00:00:00+02',NULL,'11');

-- v_pedestrians_locations_api source

CREATE OR REPLACE VIEW v_pedestrians_locations_api
AS SELECT l.location_id::character varying(5) AS location_id,
    l.location_name,
    l.lat,
    l.lng,
    l.address,
    l.city_district,
    l.tech,
    l.map_url,
    l.place_url,
    l.measurement_start,
    l.measurement_end
   FROM pedestrians_locations_api l
  ORDER BY (l.location_id::character varying(5));

-- v_pedestrians_directions_api source

CREATE OR REPLACE VIEW v_pedestrians_directions_api
AS SELECT pedestrians_directions_api.direction_id,
    pedestrians_directions_api.location_id::character varying(5) AS location_id,
    pedestrians_directions_api.direction_name,
    pedestrians_directions_api.direction_type::character varying(5) AS direction_type
   FROM pedestrians_directions_api
  ORDER BY (pedestrians_directions_api.location_id::character varying(5));

-- public.counters_detections definition

CREATE TABLE counters_detections (
	locations_id varchar(255) NOT NULL,
	directions_id varchar(255) NOT NULL,
	measured_from int8 NOT NULL,
	measured_to int8 NOT NULL,
	category varchar(100) NOT NULL,
	value int4 NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT counters_detections_pkey PRIMARY KEY (locations_id, directions_id, measured_from, category)
);
CREATE INDEX counters_detections_directions_id ON counters_detections USING btree (directions_id);
CREATE INDEX counters_detections_locations_id ON counters_detections USING btree (locations_id);

-- public.counters_directions definition

CREATE TABLE counters_directions (
	id varchar(255) NOT NULL,
	vendor_id varchar(255) NOT NULL,
	locations_id varchar(255) NOT NULL,
	name varchar(255) NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT counters_directions_pkey PRIMARY KEY (id)
);
CREATE INDEX counters_directions_locations_id ON counters_directions USING btree (locations_id);

-- public.counters_locations definition

CREATE TABLE counters_locations (
	id varchar(255) NOT NULL,
	vendor_id varchar(255) NOT NULL,
	vendor varchar(100) NULL,
	lat float8 NOT NULL,
	lng float8 NOT NULL,
	name varchar(255) NULL,
	route varchar(255) NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT counters_locations_pkey PRIMARY KEY (id)
);

-- pedestrians_wifi definition

CREATE TABLE pedestrians_wifi (
	measured_from timestamp NULL,
	start_region varchar(50) NULL,
	end_region varchar(50) NULL,
	value int4 NULL,
	direction_id varchar(50) NULL,
	location_id varchar(50) NULL
);

-- pedestrians_detections_api definition

CREATE TABLE pedestrians_detections_api (
	measured_from timestamptz NOT NULL,
	measured_to timestamptz NOT NULL,
	location_id varchar NOT NULL,
	direction_id varchar NOT NULL,
	value int8 NULL,
	quality numeric NULL,
	CONSTRAINT pedestrians_detections_api_pkey PRIMARY KEY (location_id, direction_id, measured_from, measured_to)
);

-- update_detections_data procedure definition

CREATE OR replace procedure update_detections_data()
LANGUAGE plpgsql
AS $$
declare
	lastupdatetimestamp timestamptz;
	lastupdateunix bigint;
begin
	select
			case
				when flowmax.max_measured_from is not null
				then flowmax.max_measured_from
				else to_timestamp(0)
			end as max_measured_from into lastupdatetimestamp
	from (select max(measured_from) - interval '1 hours' as max_measured_from from flow.pedestrians_detections_api) flowMax;

	lastupdateunix := extract ('epoch' from lastupdatetimestamp) * 1000;


	insert into flow.pedestrians_detections_api
	with wifi as (
		select
			pedestrians_wifi.location_id,
			pedestrians_wifi.direction_id,
			date_trunc('hour'::text, pedestrians_wifi.measured_from) + (date_part('minute'::text, pedestrians_wifi.measured_from)::integer / 15)::double precision * '00:15:00'::interval as measured_from,
			date_trunc('hour'::text, pedestrians_wifi.measured_from) + (date_part('minute'::text, pedestrians_wifi.measured_from)::integer / 15)::double precision * '00:15:00'::interval + '00:15:00'::interval as measured_to,
			sum(pedestrians_wifi.value) as value,
			count(pedestrians_wifi.value) as count_n,
			3 as quantity
		from
			flow.pedestrians_wifi
		group by
			pedestrians_wifi.location_id,
			pedestrians_wifi.direction_id,
			(date_trunc('hour'::text, pedestrians_wifi.measured_from) + (date_part('minute'::text, pedestrians_wifi.measured_from)::integer / 15)::double precision * '00:15:00'::interval),
			(date_trunc('hour'::text, pedestrians_wifi.measured_from) + (date_part('minute'::text, pedestrians_wifi.measured_from)::integer / 15)::double precision * '00:15:00'::interval + '00:15:00'::interval)
	) SELECT wifi.measured_from,
	    wifi.measured_to,
	    wifi.location_id,
	    wifi.direction_id,
	    wifi.value,
	    wifi.count_n::numeric / wifi.quantity::numeric AS quality
	FROM wifi
	where wifi.measured_from > lastupdatetimestamp
	ON CONFLICT (location_id,direction_id,measured_from,measured_to)
		DO update
		SET value = EXCLUDED.value,
		quality = EXCLUDED.quality;

	insert into	flow.pedestrians_detections_api
	with pyro as (
		select
			cd.locations_id as location_id,
			cd.directions_id as direction_id,
			to_timestamp((cd.measured_from / 1000)::double precision) as measured_from,
			to_timestamp((cd.measured_from / 1000)::double precision) + '00:15:00'::interval as measured_to,
			sum(cd.value) as value,
			1 as count_n,
			1 as quantity
		from flow.counters_detections cd
		where
			cd.category::text = 'pedestrian'::text
			and (cd.directions_id::text in (select distinct pedestrians_directions_api.direction_id	from flow.pedestrians_directions_api))
		group by
			cd.locations_id,
			cd.directions_id,
			(to_timestamp((cd.measured_from / 1000)::double precision)),
			(to_timestamp((cd.measured_from / 1000)::double precision) + '00:15:00'::interval)
	)
	select
		pyro.measured_from,
		pyro.measured_to,
		pyro.location_id,
		pyro.direction_id,
		pyro.value,
		pyro.count_n::numeric / pyro.quantity::numeric as quality
	from pyro
	where pyro.measured_from > lastupdatetimestamp
	ON CONFLICT (location_id,direction_id,measured_from,measured_to)
		DO update
		SET value = EXCLUDED.value,
		quality = EXCLUDED.quality;


	insert into	flow.pedestrians_detections_api
    with vyber as (
        select * from flow.flow_measurements where flow_measurements.start_timestamp > lastupdateunix and category::text = 'pedestrian'::text
    ), flow as (
        select
            vyber.cube_id::character varying(50) as location_id,
            vyber.sink_id::character varying(50) as direction_id,
            date_trunc('hour'::text, to_timestamp((vyber.start_timestamp / 1000)::double precision)) + (date_part('minute'::text, to_timestamp((vyber.start_timestamp / 1000)::double precision))::integer / 15)::double precision * '00:15:00'::interval as measured_from,
            date_trunc('hour'::text, to_timestamp((vyber.start_timestamp / 1000)::double precision)) + (date_part('minute'::text, to_timestamp((vyber.start_timestamp / 1000)::double precision))::integer / 15)::double precision * '00:15:00'::interval + '00:15:00'::interval as measured_to,
            sum(vyber.value) as value,
            count(vyber.value) as count_n,
            3 as quantity
        from
            vyber
        where
            ((vyber.cube_id::character varying(50)::text,
            vyber.sink_id::character varying(50)::text) in (
            select distinct
                pedestrians_directions_api.cube_id as location_id,
                pedestrians_directions_api.direction_id
            from
                flow.pedestrians_directions_api))
        group by 1,2,3,4
	) select
		flow.measured_from,
		flow.measured_to,
		flow.location_id,
		flow.direction_id,
		flow.value,
		flow.count_n::numeric / flow.quantity::numeric as quality
	from flow
	where flow.measured_from > lastupdatetimestamp
	ON CONFLICT (location_id,direction_id,measured_from,measured_to)
		DO update
		SET value = EXCLUDED.value,
		quality = EXCLUDED.quality;
end;
$$;

-- ^^
