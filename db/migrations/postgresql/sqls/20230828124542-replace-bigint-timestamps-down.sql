-- drop dependent views
drop view analytic.v_pedestrians_detections_15min;
drop view analytic.v_pedestrians_detections_api;
drop view analytic.v_pedestrians_detections_daily;
drop view analytic.v_pedestrians_flow_quality;

-- flow_sinks
alter table flow_sinks alter column history_start_timestamp type bigint using extract(epoch from history_start_timestamp) * 1000;

-- flow_od_measurements
drop table if exists flow_od_measurements_old;
alter table flow_od_measurements rename to flow_od_measurements_old;
CREATE TABLE flow_od_measurements (
	cube_id int4 NOT NULL,
	analytic_id int4 NOT NULL,
	sequence_number int4 NOT NULL,
	sink_id int4 NOT NULL,
	start_timestamp int8 NOT NULL,
	end_timestamp int8 NOT NULL,
	category varchar(150) NOT NULL,
	origin varchar(150) NOT NULL,
	destination varchar(150) NOT NULL,
	value int4 NOT NULL,
	data_validity varchar(50) NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT flow_od_measurementspkey PRIMARY KEY (cube_id, sink_id, start_timestamp, end_timestamp, category, origin, destination, sequence_number)
);
CREATE INDEX flow_od_measurements_created_at ON flow_od_measurements USING btree (created_at);

-- flow_measurements
drop table if exists flow_measurements_old;
alter table flow_measurements rename to flow_measurements_old;

DO $$
DECLARE
    partition_name text;
BEGIN
    FOR partition_name IN (SELECT relname FROM pg_class WHERE relnamespace = (SELECT current_schema() as value)::regnamespace AND relname LIKE 'flow_measurements_%' and relispartition = true and relpartbound is not null)
    LOOP
        EXECUTE format('ALTER TABLE %I.%I RENAME TO %I', (SELECT current_schema() as value), partition_name, partition_name || '_old');
    END LOOP;
END $$;

CREATE TABLE flow_measurements (
	sink_id int4 NOT NULL,
	cube_id int4 NOT NULL,
	sequence_number int4 NOT NULL,
	analytic_id int4 NOT NULL,
	start_timestamp int8 NOT NULL,
	end_timestamp int8 NOT NULL,
	category varchar(150) NOT NULL,
	value int4 NOT NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	data_validity varchar(50) NULL,
	CONSTRAINT flow_measurements_pkey PRIMARY KEY (cube_id, sink_id, start_timestamp, end_timestamp, category, sequence_number)
)
PARTITION BY RANGE (start_timestamp);
CREATE INDEX flow_measurements_created_at ON ONLY flow_measurements USING btree (created_at);
CREATE INDEX flow_measurements_start_cube_sink ON ONLY flow_measurements USING btree (cube_id, sink_id);
CREATE INDEX flow_measurements_start_timestamp ON ONLY flow_measurements USING btree (start_timestamp);

-- partitions
CREATE TABLE flow_measurements_min PARTITION OF flow_measurements FOR VALUES FROM (MINVALUE) TO (extract ('epoch' from '2020-09-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2020m09 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2020-09-01'::timestamp)*1000) TO (extract ('epoch' from '2020-10-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2020m10 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2020-10-01'::timestamp)*1000) TO (extract ('epoch' from '2020-11-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2020m11 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2020-11-01'::timestamp)*1000) TO (extract ('epoch' from '2020-12-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2020m12 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2020-12-01'::timestamp)*1000) TO (extract ('epoch' from '2021-01-01'::timestamp)*1000);
--2021
CREATE TABLE flow_measurements_y2021m01 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2021-01-01'::timestamp)*1000) TO (extract ('epoch' from '2021-02-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2021m02 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2021-02-01'::timestamp)*1000) TO (extract ('epoch' from '2021-03-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2021m03 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2021-03-01'::timestamp)*1000) TO (extract ('epoch' from '2021-04-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2021m04 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2021-04-01'::timestamp)*1000) TO (extract ('epoch' from '2021-05-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2021m05 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2021-05-01'::timestamp)*1000) TO (extract ('epoch' from '2021-06-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2021m06 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2021-06-01'::timestamp)*1000) TO (extract ('epoch' from '2021-07-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2021m07 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2021-07-01'::timestamp)*1000) TO (extract ('epoch' from '2021-08-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2021m08 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2021-08-01'::timestamp)*1000) TO (extract ('epoch' from '2021-09-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2021m09 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2021-09-01'::timestamp)*1000) TO (extract ('epoch' from '2021-10-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2021m10 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2021-10-01'::timestamp)*1000) TO (extract ('epoch' from '2021-11-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2021m11 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2021-11-01'::timestamp)*1000) TO (extract ('epoch' from '2021-12-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2021m12 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2021-12-01'::timestamp)*1000) TO (extract ('epoch' from '2022-01-01'::timestamp)*1000);
--2022
CREATE TABLE flow_measurements_y2022m01 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2022-01-01'::timestamp)*1000) TO (extract ('epoch' from '2022-02-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2022m02 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2022-02-01'::timestamp)*1000) TO (extract ('epoch' from '2022-03-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2022m03 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2022-03-01'::timestamp)*1000) TO (extract ('epoch' from '2022-04-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2022m04 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2022-04-01'::timestamp)*1000) TO (extract ('epoch' from '2022-05-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2022m05 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2022-05-01'::timestamp)*1000) TO (extract ('epoch' from '2022-06-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2022m06 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2022-06-01'::timestamp)*1000) TO (extract ('epoch' from '2022-07-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2022m07 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2022-07-01'::timestamp)*1000) TO (extract ('epoch' from '2022-08-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2022m08 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2022-08-01'::timestamp)*1000) TO (extract ('epoch' from '2022-09-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2022m09 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2022-09-01'::timestamp)*1000) TO (extract ('epoch' from '2022-10-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2022m10 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2022-10-01'::timestamp)*1000) TO (extract ('epoch' from '2022-11-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2022m11 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2022-11-01'::timestamp)*1000) TO (extract ('epoch' from '2022-12-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2022m12 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2022-12-01'::timestamp)*1000) TO (extract ('epoch' from '2023-01-01'::timestamp)*1000);
--2023
CREATE TABLE flow_measurements_y2023m01 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2023-01-01'::timestamp)*1000) TO (extract ('epoch' from '2023-02-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2023m02 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2023-02-01'::timestamp)*1000) TO (extract ('epoch' from '2023-03-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2023m03 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2023-03-01'::timestamp)*1000) TO (extract ('epoch' from '2023-04-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2023m04 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2023-04-01'::timestamp)*1000) TO (extract ('epoch' from '2023-05-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2023m05 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2023-05-01'::timestamp)*1000) TO (extract ('epoch' from '2023-06-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2023m06 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2023-06-01'::timestamp)*1000) TO (extract ('epoch' from '2023-07-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2023m07 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2023-07-01'::timestamp)*1000) TO (extract ('epoch' from '2023-08-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2023m08 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2023-08-01'::timestamp)*1000) TO (extract ('epoch' from '2023-09-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2023m09 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2023-09-01'::timestamp)*1000) TO (extract ('epoch' from '2023-10-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2023m10 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2023-10-01'::timestamp)*1000) TO (extract ('epoch' from '2023-11-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2023m11 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2023-11-01'::timestamp)*1000) TO (extract ('epoch' from '2023-12-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2023m12 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2023-12-01'::timestamp)*1000) TO (extract ('epoch' from '2024-01-01'::timestamp)*1000);
--2024
CREATE TABLE flow_measurements_y2024m01 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2024-01-01'::timestamp)*1000) TO (extract ('epoch' from '2024-02-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2024m02 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2024-02-01'::timestamp)*1000) TO (extract ('epoch' from '2024-03-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2024m03 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2024-03-01'::timestamp)*1000) TO (extract ('epoch' from '2024-04-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2024m04 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2024-04-01'::timestamp)*1000) TO (extract ('epoch' from '2024-05-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2024m05 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2024-05-01'::timestamp)*1000) TO (extract ('epoch' from '2024-06-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2024m06 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2024-06-01'::timestamp)*1000) TO (extract ('epoch' from '2024-07-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2024m07 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2024-07-01'::timestamp)*1000) TO (extract ('epoch' from '2024-08-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2024m08 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2024-08-01'::timestamp)*1000) TO (extract ('epoch' from '2024-09-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2024m09 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2024-09-01'::timestamp)*1000) TO (extract ('epoch' from '2024-10-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2024m10 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2024-10-01'::timestamp)*1000) TO (extract ('epoch' from '2024-11-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2024m11 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2024-11-01'::timestamp)*1000) TO (extract ('epoch' from '2024-12-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2024m12 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2024-12-01'::timestamp)*1000) TO (extract ('epoch' from '2025-01-01'::timestamp)*1000);
--2025
CREATE TABLE flow_measurements_y2025m01 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2025-01-01'::timestamp)*1000) TO (extract ('epoch' from '2025-02-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2025m02 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2025-02-01'::timestamp)*1000) TO (extract ('epoch' from '2025-03-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2025m03 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2025-03-01'::timestamp)*1000) TO (extract ('epoch' from '2025-04-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2025m04 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2025-04-01'::timestamp)*1000) TO (extract ('epoch' from '2025-05-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2025m05 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2025-05-01'::timestamp)*1000) TO (extract ('epoch' from '2025-06-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2025m06 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2025-06-01'::timestamp)*1000) TO (extract ('epoch' from '2025-07-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2025m07 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2025-07-01'::timestamp)*1000) TO (extract ('epoch' from '2025-08-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2025m08 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2025-08-01'::timestamp)*1000) TO (extract ('epoch' from '2025-09-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2025m09 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2025-09-01'::timestamp)*1000) TO (extract ('epoch' from '2025-10-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2025m10 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2025-10-01'::timestamp)*1000) TO (extract ('epoch' from '2025-11-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2025m11 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2025-11-01'::timestamp)*1000) TO (extract ('epoch' from '2025-12-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2025m12 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2025-12-01'::timestamp)*1000) TO (extract ('epoch' from '2026-01-01'::timestamp)*1000);
--2026
CREATE TABLE flow_measurements_y2026m01 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2026-01-01'::timestamp)*1000) TO (extract ('epoch' from '2026-02-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2026m02 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2026-02-01'::timestamp)*1000) TO (extract ('epoch' from '2026-03-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2026m03 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2026-03-01'::timestamp)*1000) TO (extract ('epoch' from '2026-04-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2026m04 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2026-04-01'::timestamp)*1000) TO (extract ('epoch' from '2026-05-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2026m05 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2026-05-01'::timestamp)*1000) TO (extract ('epoch' from '2026-06-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2026m06 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2026-06-01'::timestamp)*1000) TO (extract ('epoch' from '2026-07-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2026m07 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2026-07-01'::timestamp)*1000) TO (extract ('epoch' from '2026-08-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2026m08 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2026-08-01'::timestamp)*1000) TO (extract ('epoch' from '2026-09-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2026m09 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2026-09-01'::timestamp)*1000) TO (extract ('epoch' from '2026-10-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2026m10 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2026-10-01'::timestamp)*1000) TO (extract ('epoch' from '2026-11-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2026m11 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2026-11-01'::timestamp)*1000) TO (extract ('epoch' from '2026-12-01'::timestamp)*1000);
CREATE TABLE flow_measurements_y2026m12 PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2026-12-01'::timestamp)*1000) TO (extract ('epoch' from '2027-01-01'::timestamp)*1000);
--202
CREATE TABLE flow_measurements_y2027up PARTITION OF flow_measurements FOR VALUES FROM (extract ('epoch' from '2027-01-01'::timestamp)*1000) TO (maxvalue);

-- counters_detections
alter table counters_detections alter column measured_to type bigint using extract(epoch from measured_to) * 1000;
alter table counters_detections alter column measured_from type bigint using extract(epoch from measured_from) * 1000;

-- prodecures
CREATE OR replace procedure update_detections_data()
LANGUAGE plpgsql
AS $$
declare
	lastupdatetimestamp timestamptz;
	lastupdateunix bigint;
begin
	select
			case
				when flowmax.max_measured_from is not null
				then flowmax.max_measured_from
				else to_timestamp(0)
			end as max_measured_from into lastupdatetimestamp
	from (select max(measured_from) - interval '1 hours' as max_measured_from from flow.pedestrians_detections_api) flowMax;

	lastupdateunix := extract ('epoch' from lastupdatetimestamp) * 1000;


	insert into flow.pedestrians_detections_api
	with wifi as (
		select
			pedestrians_wifi.location_id,
			pedestrians_wifi.direction_id,
			date_trunc('hour'::text, pedestrians_wifi.measured_from) + (date_part('minute'::text, pedestrians_wifi.measured_from)::integer / 15)::double precision * '00:15:00'::interval as measured_from,
			date_trunc('hour'::text, pedestrians_wifi.measured_from) + (date_part('minute'::text, pedestrians_wifi.measured_from)::integer / 15)::double precision * '00:15:00'::interval + '00:15:00'::interval as measured_to,
			sum(pedestrians_wifi.value) as value,
			count(pedestrians_wifi.value) as count_n,
			3 as quantity
		from
			flow.pedestrians_wifi
		group by
			pedestrians_wifi.location_id,
			pedestrians_wifi.direction_id,
			(date_trunc('hour'::text, pedestrians_wifi.measured_from) + (date_part('minute'::text, pedestrians_wifi.measured_from)::integer / 15)::double precision * '00:15:00'::interval),
			(date_trunc('hour'::text, pedestrians_wifi.measured_from) + (date_part('minute'::text, pedestrians_wifi.measured_from)::integer / 15)::double precision * '00:15:00'::interval + '00:15:00'::interval)
	) SELECT wifi.measured_from,
	    wifi.measured_to,
	    wifi.location_id,
	    wifi.direction_id,
	    wifi.value,
	    wifi.count_n::numeric / wifi.quantity::numeric AS quality
	FROM wifi
	where wifi.measured_from > lastupdatetimestamp
	ON CONFLICT (location_id,direction_id,measured_from,measured_to)
		DO update
		SET value = EXCLUDED.value,
		quality = EXCLUDED.quality;

	insert into	flow.pedestrians_detections_api
	with pyro as (
		select
			cd.locations_id as location_id,
			cd.directions_id as direction_id,
			to_timestamp((cd.measured_from / 1000)::double precision) as measured_from,
			to_timestamp((cd.measured_from / 1000)::double precision) + '00:15:00'::interval as measured_to,
			sum(cd.value) as value,
			1 as count_n,
			1 as quantity
		from flow.counters_detections cd
		where
			cd.category::text = 'pedestrian'::text
			and (cd.directions_id::text in (select distinct pedestrians_directions_api.direction_id	from flow.pedestrians_directions_api))
		group by
			cd.locations_id,
			cd.directions_id,
			(to_timestamp((cd.measured_from / 1000)::double precision)),
			(to_timestamp((cd.measured_from / 1000)::double precision) + '00:15:00'::interval)
	)
	select
		pyro.measured_from,
		pyro.measured_to,
		pyro.location_id,
		pyro.direction_id,
		pyro.value,
		pyro.count_n::numeric / pyro.quantity::numeric as quality
	from pyro
	where pyro.measured_from > lastupdatetimestamp
	ON CONFLICT (location_id,direction_id,measured_from,measured_to)
		DO update
		SET value = EXCLUDED.value,
		quality = EXCLUDED.quality;


	insert into	flow.pedestrians_detections_api
    with vyber as (
        select * from flow.flow_measurements where flow_measurements.start_timestamp > lastupdateunix and category::text = 'pedestrian'::text
    ), flow as (
        select
            vyber.cube_id::character varying(50) as location_id,
            vyber.sink_id::character varying(50) as direction_id,
            date_trunc('hour'::text, to_timestamp((vyber.start_timestamp / 1000)::double precision)) + (date_part('minute'::text, to_timestamp((vyber.start_timestamp / 1000)::double precision))::integer / 15)::double precision * '00:15:00'::interval as measured_from,
            date_trunc('hour'::text, to_timestamp((vyber.start_timestamp / 1000)::double precision)) + (date_part('minute'::text, to_timestamp((vyber.start_timestamp / 1000)::double precision))::integer / 15)::double precision * '00:15:00'::interval + '00:15:00'::interval as measured_to,
            sum(vyber.value) as value,
            count(vyber.value) as count_n,
            3 as quantity
        from
            vyber
        where
            ((vyber.cube_id::character varying(50)::text,
            vyber.sink_id::character varying(50)::text) in (
            select distinct
                pedestrians_directions_api.cube_id as location_id,
                pedestrians_directions_api.direction_id
            from
                flow.pedestrians_directions_api))
        group by 1,2,3,4
	) select
		flow.measured_from,
		flow.measured_to,
		flow.location_id,
		flow.direction_id,
		flow.value,
		flow.count_n::numeric / flow.quantity::numeric as quality
	from flow
	where flow.measured_from > lastupdatetimestamp
	ON CONFLICT (location_id,direction_id,measured_from,measured_to)
		DO update
		SET value = EXCLUDED.value,
		quality = EXCLUDED.quality;
end;
$$;

-- recreate views
CREATE OR REPLACE VIEW analytic.v_pedestrians_detections_15min
AS WITH wifi AS (
         SELECT pedestrians_wifi.location_id AS locations_id,
            pedestrians_wifi.direction_id AS directions_id,
            date_trunc('hour'::text, pedestrians_wifi.measured_from) + (date_part('minute'::text, pedestrians_wifi.measured_from)::integer / 15)::double precision * '00:15:00'::interval AS measured_from,
            sum(pedestrians_wifi.value) AS value,
            count(pedestrians_wifi.value) AS count_n
           FROM flow.pedestrians_wifi
          GROUP BY pedestrians_wifi.location_id, pedestrians_wifi.direction_id, (date_trunc('hour'::text, pedestrians_wifi.measured_from) + (date_part('minute'::text, pedestrians_wifi.measured_from)::integer / 15)::double precision * '00:15:00'::interval)
        ), pyro AS (
         SELECT cd.locations_id,
            lg.first_dir AS directions_id,
            to_timestamp((cd.measured_from / 1000)::double precision) AS measured_from,
            sum(cd.value) AS value,
            count(cd.value) AS count_n
           FROM flow.counters_detections cd
             LEFT JOIN ( SELECT pedestrians_locations_gates.direction_id,
                    pedestrians_locations_gates.direction_type,
                    min(pedestrians_locations_gates.direction_id::text) OVER (PARTITION BY pedestrians_locations_gates.cube_id, pedestrians_locations_gates.direction_type) AS first_dir
                   FROM analytic.pedestrians_locations_gates) lg ON cd.directions_id::text = lg.direction_id::text
          WHERE cd.category::text = 'pedestrian'::text AND (cd.directions_id::text IN ( SELECT DISTINCT pedestrians_locations_gates.direction_id
                   FROM analytic.pedestrians_locations_gates))
          GROUP BY cd.locations_id, lg.first_dir, (to_timestamp((cd.measured_from / 1000)::double precision))
        ), flow AS (
         SELECT flow_measurements.cube_id::character varying(50) AS location_id,
            flow_measurements.sink_id::character varying(50) AS direction_id,
            date_trunc('hour'::text, to_timestamp((flow_measurements.start_timestamp / 1000)::double precision)) + (date_part('minute'::text, to_timestamp((flow_measurements.start_timestamp / 1000)::double precision))::integer / 15)::double precision * '00:15:00'::interval AS measured_from,
            sum(flow_measurements.value) AS value,
            count(flow_measurements.value) AS count_n
           FROM flow.flow_measurements
          WHERE ((flow_measurements.cube_id::character varying(50)::text, flow_measurements.sink_id::character varying(50)::text) IN ( SELECT DISTINCT pedestrians_locations_gates.cube_id AS location_id,
                    pedestrians_locations_gates.direction_id
                   FROM analytic.pedestrians_locations_gates)) AND flow_measurements.category::text = 'pedestrian'::text
          GROUP BY (flow_measurements.cube_id::character varying(50)), (flow_measurements.sink_id::character varying(50)), (date_trunc('hour'::text, to_timestamp((flow_measurements.start_timestamp / 1000)::double precision)) + (date_part('minute'::text, to_timestamp((flow_measurements.start_timestamp / 1000)::double precision))::integer / 15)::double precision * '00:15:00'::interval)
          ORDER BY (date_trunc('hour'::text, to_timestamp((flow_measurements.start_timestamp / 1000)::double precision)) + (date_part('minute'::text, to_timestamp((flow_measurements.start_timestamp / 1000)::double precision))::integer / 15)::double precision * '00:15:00'::interval), (flow_measurements.cube_id::character varying(50)), (flow_measurements.sink_id::character varying(50))
        ), measurements AS (
         SELECT wifi.locations_id,
            wifi.directions_id,
            wifi.measured_from,
            wifi.value,
            wifi.count_n
           FROM wifi
        UNION ALL
         SELECT pyro.locations_id,
            pyro.directions_id,
            pyro.measured_from,
            pyro.value,
            pyro.count_n
           FROM pyro
        UNION ALL
         SELECT flow.location_id,
            flow.direction_id,
            flow.measured_from,
            flow.value,
            flow.count_n
           FROM flow
        ), calendar AS (
         SELECT generate_series(min(measurements.measured_from), max(measurements.measured_from), '00:15:00'::interval) AS calendar
           FROM measurements
        ), classes AS (
         SELECT calendar.calendar,
            m_1.location_id,
            m_1.direction_id
           FROM calendar
             JOIN ( SELECT DISTINCT measurements.locations_id::character varying(50) AS location_id,
                    measurements.directions_id::character varying(50) AS direction_id
                   FROM measurements) m_1 ON true
        )
 SELECT c.calendar,
    date_trunc('hour'::text, c.calendar) AS calendar_hour,
    c.calendar::date AS calendar_date,
    ll.measurement_start,
    COALESCE(ll.measurement_end, now()::date::timestamp with time zone) AS measurement_end,
    c.location_id,
    c.direction_id,
    concat(c.location_id, '-', c.direction_id) AS compound_id,
    COALESCE(m.value, 0::bigint) AS value,
    COALESCE(m.count_n, 0::bigint) AS count_n
   FROM classes c
     LEFT JOIN measurements m ON c.calendar = m.measured_from AND c.location_id::text = m.locations_id::text AND c.direction_id::text = m.directions_id::text
     LEFT JOIN analytic.pedestrians_locations_list ll ON c.location_id::text = ll.cube_id::text
  WHERE c.calendar >= ll.measurement_start AND c.calendar <= COALESCE(ll.measurement_end, now()::date::timestamp with time zone);


CREATE OR REPLACE VIEW analytic.v_pedestrians_detections_api
AS WITH wifi AS (
         SELECT pedestrians_wifi.location_id,
            pedestrians_wifi.direction_id,
            date_trunc('hour'::text, pedestrians_wifi.measured_from) + (date_part('minute'::text, pedestrians_wifi.measured_from)::integer / 15)::double precision * '00:15:00'::interval AS measured_from,
            date_trunc('hour'::text, pedestrians_wifi.measured_from) + (date_part('minute'::text, pedestrians_wifi.measured_from)::integer / 15)::double precision * '00:15:00'::interval + '00:15:00'::interval AS measured_to,
            sum(pedestrians_wifi.value) AS value,
            count(pedestrians_wifi.value) AS count_n,
            3 AS quantity
           FROM flow.pedestrians_wifi
          GROUP BY pedestrians_wifi.location_id, pedestrians_wifi.direction_id, (date_trunc('hour'::text, pedestrians_wifi.measured_from) + (date_part('minute'::text, pedestrians_wifi.measured_from)::integer / 15)::double precision * '00:15:00'::interval), (date_trunc('hour'::text, pedestrians_wifi.measured_from) + (date_part('minute'::text, pedestrians_wifi.measured_from)::integer / 15)::double precision * '00:15:00'::interval + '00:15:00'::interval)
        ), pyro AS (
         SELECT cd.locations_id AS location_id,
            cd.directions_id AS direction_id,
            to_timestamp((cd.measured_from / 1000)::double precision) AS measured_from,
            to_timestamp((cd.measured_from / 1000)::double precision) + '00:15:00'::interval AS measured_to,
            sum(cd.value) AS value,
            count(cd.value) AS count_n,
            1 AS quantity
           FROM flow.counters_detections cd
          WHERE cd.category::text = 'pedestrian'::text AND (cd.directions_id::text IN ( SELECT DISTINCT pedestrians_directions_api.direction_id
                   FROM flow.pedestrians_directions_api))
          GROUP BY cd.locations_id, cd.directions_id, (to_timestamp((cd.measured_from / 1000)::double precision)), (to_timestamp((cd.measured_from / 1000)::double precision) + '00:15:00'::interval)
        ), flow AS (
         SELECT flow_measurements.cube_id::character varying(50) AS location_id,
            flow_measurements.sink_id::character varying(50) AS direction_id,
            date_trunc('hour'::text, to_timestamp((flow_measurements.start_timestamp / 1000)::double precision)) + (date_part('minute'::text, to_timestamp((flow_measurements.start_timestamp / 1000)::double precision))::integer / 15)::double precision * '00:15:00'::interval AS measured_from,
            date_trunc('hour'::text, to_timestamp((flow_measurements.start_timestamp / 1000)::double precision)) + (date_part('minute'::text, to_timestamp((flow_measurements.start_timestamp / 1000)::double precision))::integer / 15)::double precision * '00:15:00'::interval + '00:15:00'::interval AS measured_to,
            sum(flow_measurements.value) AS value,
            count(flow_measurements.value) AS count_n,
            3 AS quantity
           FROM flow.flow_measurements
          WHERE ((flow_measurements.cube_id::character varying(50)::text, flow_measurements.sink_id::character varying(50)::text) IN ( SELECT DISTINCT pedestrians_directions_api.cube_id AS location_id,
                    pedestrians_directions_api.direction_id
                   FROM flow.pedestrians_directions_api)) AND flow_measurements.category::text = 'pedestrian'::text
          GROUP BY (flow_measurements.cube_id::character varying(50)), (flow_measurements.sink_id::character varying(50)), (date_trunc('hour'::text, to_timestamp((flow_measurements.start_timestamp / 1000)::double precision)) + (date_part('minute'::text, to_timestamp((flow_measurements.start_timestamp / 1000)::double precision))::integer / 15)::double precision * '00:15:00'::interval), (date_trunc('hour'::text, to_timestamp((flow_measurements.start_timestamp / 1000)::double precision)) + (date_part('minute'::text, to_timestamp((flow_measurements.start_timestamp / 1000)::double precision))::integer / 15)::double precision * '00:15:00'::interval + '00:15:00'::interval)
          ORDER BY (date_trunc('hour'::text, to_timestamp((flow_measurements.start_timestamp / 1000)::double precision)) + (date_part('minute'::text, to_timestamp((flow_measurements.start_timestamp / 1000)::double precision))::integer / 15)::double precision * '00:15:00'::interval), (flow_measurements.cube_id::character varying(50)), (flow_measurements.sink_id::character varying(50))
        ), measurements AS (
         SELECT wifi.location_id,
            wifi.direction_id,
            wifi.measured_from,
            wifi.measured_to,
            wifi.value,
            wifi.count_n,
            wifi.quantity
           FROM wifi
        UNION ALL
         SELECT pyro.location_id,
            pyro.direction_id,
            pyro.measured_from,
            pyro.measured_to,
            pyro.value,
            pyro.count_n,
            pyro.quantity
           FROM pyro
        UNION ALL
         SELECT flow.location_id,
            flow.direction_id,
            flow.measured_from,
            flow.measured_to,
            flow.value,
            flow.count_n,
            flow.quantity
           FROM flow
        )
 SELECT measurements.measured_from,
    measurements.measured_to,
    pda.location_id::character varying(50) AS location_id,
    measurements.direction_id,
    measurements.value,
        CASE
            WHEN (measurements.count_n::numeric / measurements.quantity::numeric) > 1::numeric THEN 1::numeric
            ELSE measurements.count_n::numeric / measurements.quantity::numeric
        END AS quality
   FROM measurements
     LEFT JOIN flow.pedestrians_directions_api pda ON measurements.direction_id::text = pda.direction_id::text and measurements.location_id = pda.cube_id;


CREATE OR REPLACE VIEW analytic.v_pedestrians_detections_daily
AS WITH wifi AS (
         SELECT pedestrians_wifi.location_id AS locations_id,
            pedestrians_wifi.direction_id AS directions_id,
            pedestrians_wifi.measured_from::date AS measured_from,
            sum(pedestrians_wifi.value) AS value,
            count(pedestrians_wifi.value) AS count_n,
            count(pedestrians_wifi.value)::numeric / 288::numeric AS quality_ratio
           FROM flow.pedestrians_wifi
          GROUP BY pedestrians_wifi.location_id, pedestrians_wifi.direction_id, (pedestrians_wifi.measured_from::date)
        ), pyro AS (
         SELECT cd.locations_id,
            lg.first_dir AS directions_id,
            to_timestamp((cd.measured_from / 1000)::double precision)::date AS measured_from,
            sum(cd.value) AS value,
            count(cd.value) / count(DISTINCT cd.directions_id) AS count_n,
            count(cd.value)::numeric / 96::numeric / count(DISTINCT cd.directions_id)::numeric AS quality_ratio
           FROM flow.counters_detections cd
             LEFT JOIN ( SELECT pedestrians_locations_gates.direction_id,
                    pedestrians_locations_gates.direction_type,
                    min(pedestrians_locations_gates.direction_id::text) OVER (PARTITION BY pedestrians_locations_gates.cube_id, pedestrians_locations_gates.direction_type) AS first_dir
                   FROM analytic.pedestrians_locations_gates) lg ON cd.directions_id::text = lg.direction_id::text
          WHERE cd.category::text = 'pedestrian'::text AND (cd.directions_id::text IN ( SELECT DISTINCT pedestrians_locations_gates.direction_id
                   FROM analytic.pedestrians_locations_gates))
          GROUP BY cd.locations_id, lg.first_dir, (to_timestamp((cd.measured_from / 1000)::double precision)::date)
        ), flow AS (
         SELECT flow_measurements.cube_id::character varying(50) AS location_id,
            flow_measurements.sink_id::character varying(50) AS direction_id,
            to_timestamp((flow_measurements.start_timestamp / 1000)::double precision)::date AS measured_from,
            sum(flow_measurements.value) AS value,
            count(flow_measurements.value) AS count_n,
            count(flow_measurements.value)::numeric / 288::numeric AS quality_ratio
           FROM flow.flow_measurements
          WHERE ((flow_measurements.cube_id::character varying(50)::text, flow_measurements.sink_id::character varying(50)::text) IN ( SELECT DISTINCT pedestrians_locations_gates.cube_id AS location_id,
                    pedestrians_locations_gates.direction_id
                   FROM analytic.pedestrians_locations_gates)) AND flow_measurements.category::text = 'pedestrian'::text
          GROUP BY (flow_measurements.cube_id::character varying(50)), (flow_measurements.sink_id::character varying(50)), (to_timestamp((flow_measurements.start_timestamp / 1000)::double precision)::date)
          ORDER BY (to_timestamp((flow_measurements.start_timestamp / 1000)::double precision)::date), (flow_measurements.cube_id::character varying(50)) DESC, (flow_measurements.sink_id::character varying(50))
        ), measurements AS (
         SELECT pyro.locations_id,
            pyro.directions_id,
            pyro.measured_from,
            pyro.value,
            pyro.count_n,
            pyro.quality_ratio
           FROM pyro
        UNION ALL
         SELECT flow.location_id,
            flow.direction_id,
            flow.measured_from,
            flow.value,
            flow.count_n,
            flow.quality_ratio
           FROM flow
        UNION ALL
         SELECT wifi.locations_id,
            wifi.directions_id,
            wifi.measured_from,
            wifi.value,
            wifi.count_n,
            wifi.quality_ratio
           FROM wifi
        ), calendar AS (
         SELECT generate_series(min(measurements.measured_from)::timestamp with time zone, max(measurements.measured_from)::timestamp with time zone, '1 day'::interval) AS calendar
           FROM measurements
        ), classes AS (
         SELECT calendar.calendar,
            m_1.location_id,
            m_1.direction_id
           FROM calendar
             JOIN ( SELECT DISTINCT measurements.locations_id::character varying(50) AS location_id,
                    measurements.directions_id::character varying(50) AS direction_id
                   FROM measurements) m_1 ON true
        )
 SELECT c.calendar,
    c.location_id,
    c.direction_id,
    concat(c.location_id, '-', c.direction_id) AS compound_id,
    COALESCE(m.value, 0::bigint) AS value,
    COALESCE(m.count_n, 0::bigint) AS count_n,
    COALESCE(m.quality_ratio, 0::numeric) AS quality_ratio,
        CASE
            WHEN c.calendar < ll.measurement_start OR c.calendar > ll.measurement_end THEN 4
            WHEN COALESCE(m.quality_ratio, 0::numeric) = 0::numeric THEN 3
            WHEN COALESCE(m.quality_ratio, 0::numeric) < 0.9 THEN 2
            WHEN COALESCE(m.quality_ratio, 0::numeric) > 0.9 AND COALESCE(m.quality_ratio, 0::numeric) < 1::numeric THEN 1
            WHEN COALESCE(m.quality_ratio, 0::numeric) >= 1::numeric THEN 0
            ELSE NULL::integer
        END AS quality_code,
        CASE
            WHEN c.calendar < ll.measurement_start OR c.calendar > ll.measurement_end THEN 'Nenainstalovaná technologie'::text
            WHEN COALESCE(m.quality_ratio, 0::numeric) = 0::numeric THEN 'Bez dat (0%)'::text
            WHEN COALESCE(m.quality_ratio, 0::numeric) < 0.9 THEN 'Částečná data (<90%)'::text
            WHEN COALESCE(m.quality_ratio, 0::numeric) > 0.9 AND COALESCE(m.quality_ratio, 0::numeric) < 1::numeric THEN 'Nekompletní data (>90%)'::text
            WHEN COALESCE(m.quality_ratio, 0::numeric) >= 1::numeric THEN 'Kompletní data (100%)'::text
            ELSE NULL::text
        END AS quality_status
   FROM classes c
     LEFT JOIN measurements m ON c.calendar = m.measured_from AND c.location_id::text = m.locations_id::text AND c.direction_id::text = m.directions_id
     LEFT JOIN analytic.pedestrians_locations_list ll ON c.location_id::text = ll.cube_id::text;


CREATE OR REPLACE VIEW analytic.v_pedestrians_flow_quality
AS WITH timeline_category AS (
         SELECT DISTINCT fs.cube_id,
            fs.id AS sink_id,
            timeline."timestamp"
           FROM flow_sinks fs
             LEFT JOIN ( SELECT generate_series(date_trunc('month'::text, CURRENT_DATE - '180 days'::interval)::timestamp with time zone, date_trunc('hour'::text, CURRENT_DATE::timestamp with time zone), '01:00:00'::interval) AS "timestamp") timeline ON true
        ), real_nrow AS (
         SELECT date_trunc('hour'::text, to_timestamp((flow_measurements.start_timestamp / 1000)::double precision)) AS start_timestamp,
            flow_measurements.cube_id,
            flow_measurements.sink_id,
            count(*) AS n_row,
            count(*) FILTER (WHERE flow_measurements.category::text = 'pedestrian'::text) AS n_row_pedestrian
           FROM flow_measurements
          WHERE flow_measurements.start_timestamp::double precision >= (1000::double precision * date_part('epoch'::text, CURRENT_DATE - '210 days'::interval))
          GROUP BY (date_trunc('hour'::text, to_timestamp((flow_measurements.start_timestamp / 1000)::double precision))), flow_measurements.cube_id, flow_measurements.sink_id
        )
 SELECT t."timestamp",
    date_trunc('month'::text, t."timestamp")::date AS month,
    t.cube_id,
    t.sink_id,
    concat(t.cube_id, ' - ', t.sink_id) AS cube_sink,
    COALESCE(r.n_row::numeric, 0::bigint::numeric) AS n_row,
    COALESCE(r.n_row_pedestrian::numeric, 0::bigint::numeric) AS n_row_pedestrian,
    108::bigint AS expected_nrow, -- 9 categories * 12 measurements/hr
    12 AS expected_nrow_pedestrian -- 1 category * 12 measurements/hr
   FROM timeline_category t
     LEFT JOIN real_nrow r ON t."timestamp" = r.start_timestamp AND t.cube_id = r.cube_id AND t.sink_id = r.sink_id;


CREATE OR REPLACE VIEW analytic.v_pedestrians_flow_quality
AS WITH timeline_category AS (
         SELECT DISTINCT fs.cube_id,
            fs.id AS sink_id,
            timeline."timestamp"
           FROM flow_sinks fs
             LEFT JOIN ( SELECT generate_series(date_trunc('month'::text, CURRENT_DATE - '180 days'::interval)::timestamp with time zone, date_trunc('hour'::text, CURRENT_DATE::timestamp with time zone), '01:00:00'::interval) AS "timestamp") timeline ON true
        ), real_nrow AS (
         SELECT date_trunc('hour'::text, to_timestamp((flow_measurements.start_timestamp / 1000)::double precision)) AS start_timestamp,
            flow_measurements.cube_id,
            flow_measurements.sink_id,
            count(*) AS n_row,
            count(*) FILTER (WHERE flow_measurements.category::text = 'pedestrian'::text) AS n_row_pedestrian
           FROM flow_measurements
          WHERE flow_measurements.start_timestamp::double precision >= (1000::double precision * date_part('epoch'::text, CURRENT_DATE - '210 days'::interval))
          GROUP BY (date_trunc('hour'::text, to_timestamp((flow_measurements.start_timestamp / 1000)::double precision))), flow_measurements.cube_id, flow_measurements.sink_id
        )
 SELECT t."timestamp",
    date_trunc('month'::text, t."timestamp")::date AS month,
    t.cube_id,
    t.sink_id,
    concat(t.cube_id, ' - ', t.sink_id) AS cube_sink,
    COALESCE(r.n_row::numeric, 0::bigint::numeric) AS n_row,
    COALESCE(r.n_row_pedestrian::numeric, 0::bigint::numeric) AS n_row_pedestrian,
    108::bigint AS expected_nrow, -- 9 categories * 12 measurements/hr
    12 AS expected_nrow_pedestrian -- 1 category * 12 measurements/hr
   FROM timeline_category t
     LEFT JOIN real_nrow r ON t."timestamp" = r.start_timestamp AND t.cube_id = r.cube_id AND t.sink_id = r.sink_id;
