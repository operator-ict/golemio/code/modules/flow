CREATE OR REPLACE VIEW analytic.v_pedestrians_flow_quality
AS WITH timeline_category AS (
         SELECT DISTINCT fs.cube_id,
            fs.id AS sink_id,
            timeline."timestamp"
           FROM flow_sinks fs
             LEFT JOIN ( SELECT generate_series(date_trunc('month'::text, CURRENT_DATE - '180 days'::interval)::timestamp with time zone, date_trunc('hour'::text, CURRENT_DATE::timestamp with time zone), '01:00:00'::interval) AS "timestamp") timeline ON true
        ), real_nrow AS (
         SELECT date_trunc('hour'::text, to_timestamp((flow_measurements.start_timestamp / 1000)::double precision)) AS start_timestamp,
            flow_measurements.cube_id,
            flow_measurements.sink_id,
            count(*) AS n_row,
            count(*) FILTER (WHERE flow_measurements.category::text = 'pedestrian'::text) AS n_row_pedestrian
           FROM flow_measurements
          WHERE flow_measurements.start_timestamp::double precision >= (1000::double precision * date_part('epoch'::text, CURRENT_DATE - '210 days'::interval))
          GROUP BY (date_trunc('hour'::text, to_timestamp((flow_measurements.start_timestamp / 1000)::double precision))), flow_measurements.cube_id, flow_measurements.sink_id
        )
 SELECT t."timestamp",
    date_trunc('month'::text, t."timestamp")::date AS month,
    t.cube_id,
    t.sink_id,
    concat(t.cube_id, ' - ', t.sink_id) AS cube_sink,
    COALESCE(r.n_row::numeric, 0::bigint::numeric) AS n_row,
    COALESCE(r.n_row_pedestrian::numeric, 0::bigint::numeric) AS n_row_pedestrian,
    108::bigint AS expected_nrow, -- 9 categories * 12 measurements/hr
    12 AS expected_nrow_pedestrian -- 1 category * 12 measurements/hr
   FROM timeline_category t
     LEFT JOIN real_nrow r ON t."timestamp" = r.start_timestamp AND t.cube_id = r.cube_id AND t.sink_id = r.sink_id;
