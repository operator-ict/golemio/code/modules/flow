import { PedestriansLocationsModel } from "#og";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon from "sinon";

chai.use(chaiAsPromised);

describe("PedestriansLocationsModel", () => {
    let pedestriansLocationsModel: PedestriansLocationsModel;
    let sandbox: any = null;

    before(() => {
        sandbox = sinon.createSandbox();
        pedestriansLocationsModel = new PedestriansLocationsModel();
    });

    after(() => {
        sandbox && sandbox.restore();
    });

    it("should instantiate", () => {
        expect(pedestriansLocationsModel).not.to.be.undefined;
    });

    it("should return all items", async () => {
        const result = await pedestriansLocationsModel.GetAll();
        expect(result).to.be.an.instanceOf(Object);
        expect(result.length).to.be.eq(9);
    });
});
