import { pedestriansRouter } from "#og/PedestriansRouter";
import { log } from "@golemio/core/dist/output-gateway/Logger";
import express, { NextFunction, Request, Response } from "@golemio/core/dist/shared/express";
import { HTTPErrorHandler, IGolemioError } from "@golemio/core/dist/shared/golemio-errors";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import request from "supertest";

chai.use(chaiAsPromised);

describe("Pedestrians router test", () => {
    const app = express();

    before(() => {
        app.use("/pedestrians", pedestriansRouter);
        app.use((err: any, req: Request, res: Response, next: NextFunction) => {
            const errObject: IGolemioError = HTTPErrorHandler.handle(err, log);
            log.silly("Error caught by the router error handler.");
            res.setHeader("Content-Type", "application/json; charset=utf-8");
            res.status(errObject.error_status || 500).send(errObject);
        });
    });

    it("should respond with correctly to GET /pedestrians/locations", (done) => {
        request(app)
            .get("/pedestrians/locations?locationId=9")
            .then((response: any) => {
                expect(response.body).to.deep.equal([
                    {
                        id: "9",
                        location_name: "Železniční most - náplavka II",
                        lat: "50.06693",
                        lng: "14.41480",
                        address: "Novoměstská náplavka",
                        city_district: "Praha 2",
                        tech: "kamera",
                        map_url: "https://storage.golemio.cz/intenzita-pesi-dopravy/7-map.png",
                        photo_url: "https://storage.golemio.cz/intenzita-pesi-dopravy/7.png",
                        measurement_start: "2020-09-01 22:00",
                        measurement_end: null,
                        directions: [
                            { id: "29", name: "Plný profil, směr Vyšehrad", to: 2 },
                            { id: "35", name: "Plný profil, směr centrum", to: 1 },
                        ],
                    },
                ]);

                expect(response.headers["cache-control"]).to.equal("public, s-maxage=3600, stale-while-revalidate=300");
                done();
            })
            .catch((error) => {
                console.log(error);
                done();
            });
    });

    it("should respond correctly to GET /pedestrians/measurements", (done) => {
        request(app)
            .get("/pedestrians/measurements?locationId=9&directionId=29&from=2022-03-19T17:45:00Z&to=2022-03-19T18:00:00Z")
            .expect(200)
            .then((response: any) => {
                expect(response.body).to.deep.equal([
                    {
                        measured_from: "2022-03-19T17:45:00.000Z",
                        measured_to: "2022-03-19T18:00:00.000Z",
                        location_id: "9",
                        direction_id: "29",
                        value: "7",
                        quality: "0.66666666666666666667",
                    },
                ]);

                expect(response.headers["cache-control"]).to.equal("public, s-maxage=3600, stale-while-revalidate=300");
                done();
            })
            .catch((error) => {
                console.log(error);
                done();
            });
    });
});
