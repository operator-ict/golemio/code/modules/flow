import { PedestriansLocationsModel, PedestriansMeasurementsModel } from "#og";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon from "sinon";

chai.use(chaiAsPromised);

describe("PedestriansMeasurementsModel", () => {
    let pedestriansMeasurementsModel: PedestriansMeasurementsModel;
    let sandbox: any = null;

    before(() => {
        sandbox = sinon.createSandbox();
        pedestriansMeasurementsModel = new PedestriansMeasurementsModel();
    });

    after(() => {
        sandbox && sandbox.restore();
    });

    it("should instantiate", () => {
        expect(pedestriansMeasurementsModel).not.to.be.undefined;
    });

    it("should return WIFI data with directionId dir1", async () => {
        const result = await pedestriansMeasurementsModel.GetAll({ directionId: "dir1" });
        expect(result).to.be.an.instanceOf(Object);
        expect(result.length).to.be.eq(9);
    });

    it("should return PYRO data with directionId ecoCounter-104061215", async () => {
        const result = await pedestriansMeasurementsModel.GetAll({
            directionId: "ecoCounter-104061215",
            from: "2020-07-23T02:00:00Z",
            to: "2020-07-23T03:00:00Z",
        });
        expect(result).to.be.an.instanceOf(Object);
        expect(result.length).to.be.eq(4);
    });
});
