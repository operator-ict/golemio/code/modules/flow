import { EcoCounterMeasurementsTransformation } from "#ie/transformations/EcoCounterMeasurementsTransformation";
import { Counters } from "#sch";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import fs from "fs";

chai.use(chaiAsPromised);
const readFile = (file: string): Promise<Buffer> => {
    return new Promise((resolve, reject) => {
        const stream = fs.createReadStream(file);
        const chunks: any[] = [];

        stream.on("error", (err) => {
            reject(err);
        });
        stream.on("data", (data) => {
            chunks.push(data);
        });
        stream.on("close", () => {
            resolve(Buffer.concat(chunks));
        });
    });
};

describe("EcoCounterMeasurementsTransformation", () => {
    let transformation: EcoCounterMeasurementsTransformation;
    let testSourceData: any[];
    let detectionsValidator: JSONSchemaValidator;

    before(() => {
        detectionsValidator = new JSONSchemaValidator(
            Counters.detections.name + "ModelValidator",
            Counters.detections.outputJsonSchemaObject
        );
    });

    beforeEach(async () => {
        transformation = new EcoCounterMeasurementsTransformation();
        const buffer = await readFile(__dirname + "/data/bicyclecounters-ecocounter-measurements-datasource.json");
        testSourceData = JSON.parse(Buffer.from(buffer).toString("utf8"));
    });

    it("should has name", async () => {
        expect(transformation.name).not.to.be.undefined;
        expect(transformation.name).is.equal("EcoCounterCountersMeasurementsTransformation");
    });

    it("should has transform method", async () => {
        expect(transformation.transform).not.to.be.undefined;
    });

    it("should properly transform collection", async () => {
        let data = await transformation.transform(testSourceData);
        data = data.map((x: Record<string, string>) => {
            x.directions_id = "ecoCounter-103047647";
            x.locations_id = "ecoCounter-100047647";
            return x;
        });

        await expect(detectionsValidator.Validate(data)).to.be.fulfilled;
    });
});
