# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [1.3.3] - 2024-10-24

### Fixed

-   asyncAPI prefixes

## [1.3.2] - 2024-10-22

### Added

-   asyncAPI docs file ([integration-engine#261](https://gitlab.com/operator-ict/golemio/code/integration-engine/-/issues/261))

## [1.3.1] - 2024-08-16

### Added

-   add backstage metadata files
-   add .gitattributes file

## [1.3.0] - 2024-06-03

### Added

-   add cache-control header to all responses ([core#106](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/106))

### Removed

-   remove redis useCacheMiddleware ([core#106](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/106))

## [1.2.13] - 2024-05-13

### Changed

-   Update Node.js to v20.12.2 Express to v4.19.2 ([core#102](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/102))

## [1.2.12] - 2024-04-24

### Removed

-   Removed axios type

## [1.2.11] - 2024-04-08

### Changed

-   migration from axios to fetch/undici ([core#99](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/99))

## [1.2.10] - 2024-03-25

-   Fixed router query validation rules ([core#93](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/93))

## [1.2.9] - 2023-10-16

### Fixed

-   Fix: přidáno "if exists" do migrace, aby nepadala ([transformations#5](https://gitlab.com/operator-ict/golemio/extras/transformations/-/issues/5))

## [1.2.8] - 2023-10-16

### Changed

-   odstranění views ze schématu analytic

## [1.2.7] - 2023-09-27

### Changed

-   API versioning - version moved to path ([core#80](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/80))

## [1.2.6] - 2023-09-04

### Changed

-   Replace bigint timestamps ([flow#10](https://gitlab.com/operator-ict/golemio/code/modules/flow/-/issues/10))

## [1.2.5] - 2023-08-09

### Fixed

-   `location_id` pairing on `/pedestrians/measurements` endpoint ([pesi-doprava#80](https://gitlab.com/operator-ict/golemio/projekty/oict/pesi-doprava/-/issues/80))

## [1.2.4] - 2023-07-31

### Changed

-   Remove momentJS

## [1.2.3] - 2023-06-14

### Changed

-   Typescript version update to 5.1.3 ([core#70](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/70))

## [1.2.2] - 2023-05-31

### Changed

-   Update golemio errors ([core#62](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/62))

## [1.2.1] - 2023-02-27

### Changed

-   Update Node.js to v18.14.0, Express to v4.18.2 ([core#50](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/50))

## [1.2.0] - 2023-01-23

### Changed

-   Migrate to npm

## [1.1.0] - 2022-11-29

### Added

-   Input data and db model validation [IE254](https://gitlab.com/operator-ict/golemio/code/integration-engine/-/issues/254)

### Changed

-   Bump typescript, typedoc

## [1.0.11] - 2022-10-20

### Changed

-   Corrected the join conditions for analytic view v_pedestrians_detections_api

## [1.0.10] - 2022-08-24

### Changed

-   Corrected the callculation in analytic view v_pedestrian_flow_quality

## [1.0.9] - 2022-08-01

-   Minor migrations consistency fix

## [1.0.8] - 2022-07-18

### Changed

-   Data moved to new schema in postgres db.
-   Module counters merged in this module.

## [1.0.7] - 2022-06-21

### Changed

-   Typescript version update from 4.4.4 to 4.6.4

## [1.0.6] - 2022-03-01

### Added

-   openapi: 3.0.3 doc

### Changed

-   import Axios from Core
