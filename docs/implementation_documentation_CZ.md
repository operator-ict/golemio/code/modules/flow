# Implementační dokumentace modulu *flow*

## Záměr

Stručný popis, k čemu modul slouží.

Počítání pěších (a případně dalších detekovaných objektů, jako jsou vozidla, kola, motocykly) nad streamy kamer z MKS prostřenictvím videoanalytické aplikace FLOW od RCE Systems

## Vstupní data

### Data aktivně stahujeme

Popis dat, jak je pomocí cronu a integration-engine stahujeme. Pokud jsou data stahována z více zdrojů, každý zdroj je popsán zvlášť.

#### *FlowAPI*

- zdroj dat
  - url https://10.200.4.4:8080/api/cubes
  - bearer auth
  - header Accept-Version: 1.3
- formát dat
  - protokol HTTP get
  - datový typ json stream
- frekvence stahování
  - cron definice 0 6,26,46 * * * *
- název rabbitmq fronty
  - popis / název fronty, která vyvolá proces integrace, ve které je zdroj dat obsažen
    - dataplatform.flowmeasurements.refreshCubes



## Zpracování dat / transformace

Popis transformace a případného obohacení dat. Zaměřeno hlavně na workery a jejich metody.

### *FlowWorker*

Stručný popis workeru.

#### *refreshCubes*

- vstupní rabbitmq fronta
  - název dataplatform.flowmeasurements.refreshCubes
  - zpráva vyvolana cronem je prázdná, ale také může obsahovat json s from a to v ISO 8601 formátu. Také lze jak vstup použít nějkolik dvojic from a to v poli.
- (volitelně) závislé fronty (do kterých jsou odesílány zprávy z metody workeru)
  - název dataplatform.flowmeasurements.getAnalytics
  - ve zprávě se posílá každá cube zvlášť případně obohacena o pole intervalů from a to
- datové zdroje
  - cubes z FlowAPI
- data modely
  - výčet datových modelů, kam se data ukládájí
  - flowCubesModel - uklada definice cube do flow_cubes

#### *getAnalytics*

- vstupní rabbitmq fronta
  - název dataplatform.flowmeasurements.getAnalytics
  - zpráva obsahuje cube a pole intervalů from to
- (volitelně) závislé fronty (do kterých jsou odesílány zprávy z metody workeru)
  - název dataplatform.flowmeasurements.getSinks
  - ve zprávě se posílá celá cube zpříchozí zpravy, stažená analytic.data a intervaly from a to
- datové zdroje
  - GET FlowAPI/:cube.id/analytics
  - GET FlowAPI/:cube.id/analytics/:analytic.id
- data modely
  - v této fázi se do databáze nic neukládá

#### *getSinks*

- vstupní rabbitmq fronta
  - název dataplatform.flowmeasurements.getSinks
  - zpráva obsahuje cube, analytic a pole intervalů from to ziskané v přechozích částech
- (volitelně) závislé fronty (do kterých jsou odesílány zprávy z metody workeru)
  - název
    dataplatform.flowmeasurements.getSinksHistoryPayloads
    - příchozí zpráva se obohacuje o sinks a value type sinku
- datové zdroje
  - GET FlowAPI/:cube.id/analytics/:analytic.id/sinks
- data modely
  - výčet datových modelů, kam se data ukládájí
  - flowSinksModel - aktualizuje číselník Sinks (?senzorů?)

#### *getSinksHistoryPayloads*

- vstupní rabbitmq fronta
  - název dataplatform.flowmeasurements.getSinksHistoryPayloads
  - zpráva obsahuje cube, analytic, sinks, sink type a pole intervalů from to ziskané v přechozích částech
- (volitelně) závislé fronty (do kterých jsou odesílány zprávy z metody workeru)
  - název
    dataplatform.flowmeasurements.getSinksHistory
    - zpráva obsahuje analytic, cube, sink type a payload (payload je vytvořen za pomocí hodnot z časových intervalů, analytic sequence number a indetifikatoru)
- datové zdroje
  - nic se nestahuje
- data modely
  - nic se neukládá
- účel
  - zpráva se rozdrobí na atomické zprávy pro jednotlivé sinky a time framy

#### *getSinksHistory*

- vstupní rabbitmq fronta
  - název dataplatform.flowmeasurements.getSinksHistory
  - zpráva obsahuje cube, analytic, payload, sink type
- datové zdroje
  - POST FlowAPI/:cube.id/analytics/:analytic.id/sinks/history
    body ~ payload
- data modely
  - flowMeasurementModel
    - pro typ sinku distribution
    - datová tabulka s měřením průchodu/ průjezdu entit (chodec, kolo ...)
    - jméno tabulky flow_measurements
  - flowODMeasurementModel
    - pro typ sinku od_matrix
    - datová tabulka s měřením průchodu/ průjezdu entit včetně směru pohybu (sloupce origin destination)
    - jméno tabulky flow_od_measurements
## Uložení dat

Popis ukládání dat.

### Obecné

- typ databáze
  - PSQL
- databázové schéma
  - popis tabulek nebo odkaz na obrázek s databázovým schématem
  - ![flow er diagram](assets/flow_erd.png)
- retence dat
  - data jsou zachována

## Output API

Popis output-api.

### Obecné

- OpenAPI v3 dokumentace
  - [odkaz na dokumentaci](./openapi.yaml)
- veřejné / neveřejné endpointy
  - api je neveřejné

#### */locations*

- zdrojové view v_pedestrians_locations_api
    - používá tabulku pedestrians_locations_api
- obohaceno o data z view v_pedestrians_directions_api
    - používá tabulku pedestrians_directions_api

#### */measurements*

- zdrojové tabulka pedestrians_detections_api
    - plněna procedurou update_detections_data a získává data z:
        - pedestrians_wifi
        - counters_detections
        - pedestrians_directions_api
        - flow_measurements ~ pouze categorie pedestrian


