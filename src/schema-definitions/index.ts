import Sequelize from "@golemio/core/dist/shared/sequelize";

// Detections
const outputMeasurementsSDMA: Sequelize.ModelAttributes<any> = {
    sink_id: {
        primaryKey: true,
        type: Sequelize.BIGINT,
    },
    cube_id: {
        primaryKey: true,
        type: Sequelize.BIGINT,
    },
    sequence_number: {
        primaryKey: true,
        type: Sequelize.BIGINT,
    },
    analytic_id: { type: Sequelize.BIGINT },
    start_timestamp: {
        primaryKey: true,
        type: Sequelize.DATE,
    },
    end_timestamp: {
        primaryKey: true,
        type: Sequelize.DATE,
    },
    category: {
        primaryKey: true,
        type: Sequelize.STRING(150),
    },
    value: Sequelize.INTEGER,
    data_validity: Sequelize.STRING(50),

    // Audit database fields
    create_batch_id: { type: Sequelize.BIGINT }, // ID of input batch
    created_at: { type: Sequelize.DATE }, // Time of insertion
    created_by: { type: Sequelize.STRING(150) }, // Author or name of Worker of inserion
    update_batch_id: { type: Sequelize.BIGINT }, // ID last input batch which caused update
    updated_at: { type: Sequelize.DATE }, // Time of update
    updated_by: { type: Sequelize.STRING(150) }, // Author or name of Worker of update
};

const outputMeasurementsJsonSchema = {
    type: "array",
    items: {
        type: "object",
        properties: {
            sink_id: { type: "number" },
            cube_id: { type: "number" },
            sequence_number: { type: "number" },
            analytic_id: { type: "number" },
            start_timestamp: { type: "object", required: ["toISOString"] },
            end_timestamp: { type: "object", required: ["toISOString"] },
            category: { type: "string" },
            value: { type: "number" },
            data_validity: { type: "string" },
        },
    },
};

const outputODMeasurementsSDMA: Sequelize.ModelAttributes<any> = {
    sink_id: {
        primaryKey: true,
        type: Sequelize.BIGINT,
    },
    cube_id: {
        primaryKey: true,
        type: Sequelize.BIGINT,
    },
    sequence_number: {
        primaryKey: true,
        type: Sequelize.BIGINT,
    },
    analytic_id: { type: Sequelize.BIGINT },
    start_timestamp: {
        primaryKey: true,
        type: Sequelize.DATE,
    },
    end_timestamp: {
        primaryKey: true,
        type: Sequelize.DATE,
    },
    category: {
        primaryKey: true,
        type: Sequelize.STRING(150),
    },
    origin: {
        primaryKey: true,
        type: Sequelize.STRING(150),
    },
    destination: {
        primaryKey: true,
        type: Sequelize.STRING(150),
    },
    value: Sequelize.INTEGER,
    data_validity: Sequelize.STRING(50),

    // Audit database fields
    create_batch_id: { type: Sequelize.BIGINT }, // ID of input batch
    created_at: { type: Sequelize.DATE }, // Time of insertion
    created_by: { type: Sequelize.STRING(150) }, // Author or name of Worker of inserion
    update_batch_id: { type: Sequelize.BIGINT }, // ID last input batch which caused update
    updated_at: { type: Sequelize.DATE }, // Time of update
    updated_by: { type: Sequelize.STRING(150) }, // Author or name of Worker of update
};

const outputODMeasurementsJsonSchema = {
    type: "array",
    items: {
        type: "object",
        properties: {
            sink_id: { type: "number" },
            cube_id: { type: "number" },
            sequence_number: { type: "number" },
            analytic_id: { type: "number" },
            start_timestamp: { type: "object", required: ["toISOString"] },
            end_timestamp: { type: "object", required: ["toISOString"] },
            category: { type: "string" },
            origin: { type: "string" },
            destination: { type: "string" },
            value: { type: "number" },
            data_validity: { type: "string" },
        },
    },
};

// Detections
const outputCubesSDMA: Sequelize.ModelAttributes<any> = {
    id: {
        primaryKey: true,
        type: Sequelize.BIGINT,
    },
    name: {
        primaryKey: true,
        type: Sequelize.STRING(150),
    },

    // Audit database fields
    create_batch_id: { type: Sequelize.BIGINT }, // ID of input batch
    created_at: { type: Sequelize.DATE }, // Time of insertion
    created_by: { type: Sequelize.STRING(150) }, // Author or name of Worker of inserion
    update_batch_id: { type: Sequelize.BIGINT }, // ID last input batch which caused update
    updated_at: { type: Sequelize.DATE }, // Time of update
    updated_by: { type: Sequelize.STRING(150) }, // Author or name of Worker of update
};

const outputCubesJsonSchema = {
    type: "array",
    items: {
        type: "object",
        properties: {
            id: { type: "number" },
            name: { type: "string" },
        },
    },
};

// Detections
const outputSinksSDMA: Sequelize.ModelAttributes<any> = {
    cube_id: {
        primaryKey: true,
        type: Sequelize.BIGINT,
    },
    history_start_timestamp: {
        type: Sequelize.DATE,
    },
    id: {
        primaryKey: true,
        type: Sequelize.BIGINT,
    },
    name: {
        type: Sequelize.STRING(150),
    },
    output_value_type: {
        primaryKey: true,
        type: Sequelize.STRING(150),
    },

    // Audit database fields
    create_batch_id: { type: Sequelize.BIGINT }, // ID of input batch
    created_at: { type: Sequelize.DATE }, // Time of insertion
    created_by: { type: Sequelize.STRING(150) }, // Author or name of Worker of inserion
    update_batch_id: { type: Sequelize.BIGINT }, // ID last input batch which caused update
    updated_at: { type: Sequelize.DATE }, // Time of update
    updated_by: { type: Sequelize.STRING(150) }, // Author or name of Worker of update
};

const outputSinksJsonSchema = {
    type: "array",
    items: {
        type: "object",
        properties: {
            cube_id: { type: "number" },
            history_start_timestamp: { type: "object", required: ["toISOString"] },
            id: { type: "number" },
            name: { type: "string" },
            naoutput_value_typeme: { type: "string" },
        },
    },
};

const outputPedestriansMeasurementsSDMA: Sequelize.ModelAttributes<any> = {
    measured_from: Sequelize.DATE,
    measured_to: Sequelize.DATE,
    location_id: Sequelize.STRING(50),
    direction_id: Sequelize.STRING(50),
    value: Sequelize.BIGINT,
    quality: Sequelize.DECIMAL,
};

const outputPedestriansLocationsSDMA: Sequelize.ModelAttributes<any> = {
    location_id: Sequelize.STRING(50),
    location_name: Sequelize.STRING(250),
    lat: Sequelize.DECIMAL,
    lng: Sequelize.DECIMAL,
    address: Sequelize.STRING(250),
    city_district: Sequelize.STRING(250),
    tech: Sequelize.STRING(250),
    map_url: Sequelize.STRING(2500),
    place_url: Sequelize.STRING(2500),
    measurement_start: Sequelize.DATE,
    measurement_end: Sequelize.DATE,
};

const forExport = {
    pgSchema: "flow",
    cubes: {
        name: "FlowCubes",
        outputSequelizeAttributes: outputCubesSDMA,
        pgTableName: "flow_cubes",
        jsonSchema: outputCubesJsonSchema,
    },
    detections: {
        name: "FlowMeasurements",
        outputSequelizeAttributes: outputMeasurementsSDMA,
        pgTableName: "flow_measurements",
        jsonSchema: outputMeasurementsJsonSchema,
    },

    detectionsOD: {
        name: "FlowMeasurementsOD",
        outputSequelizeAttributes: outputODMeasurementsSDMA,
        pgTableName: "flow_od_measurements",
        jsonSchema: outputODMeasurementsJsonSchema,
    },

    sinks: {
        name: "FlowSinks",
        outputSequelizeAttributes: outputSinksSDMA,
        pgTableName: "flow_sinks",
        jsonSchema: outputSinksJsonSchema,
    },
    pedestriansMeasurementsApi: {
        name: "PedestriansMeasurementsApi",
        pgTableName: "pedestrians_detections_api",
        outputSequelizeAttributes: outputPedestriansMeasurementsSDMA,
    },
    pedestriansLocationsApi: {
        name: "PedestriansLocationsApi",
        pgTableName: "v_pedestrians_locations_api",
        outputSequelizeAttributes: outputPedestriansLocationsSDMA,
    },
};

const datasourceEcoCounterJsonSchema = {
    type: "array",
    items: {
        type: "object",
        properties: {
            channels: {
                type: "array",
                items: {
                    type: "object",
                    properties: {
                        channels: { type: ["array", "null"] },
                        counter: { type: "string" },
                        domain: { type: "string" },
                        id: { type: "number" },
                        installationDate: { type: "string" },
                        interval: { type: "number" },
                        latitude: { type: "number" },
                        longitude: { type: "number" },
                        name: { type: "string" },
                        photos: { type: ["array", "null"] },
                        sens: { type: "number" },
                        timezone: { type: "string" },
                        userType: { type: "number" },
                    },
                    required: [
                        "counter",
                        "domain",
                        "id",
                        "installationDate",
                        "interval",
                        "latitude",
                        "longitude",
                        "name",
                        "sens",
                        "timezone",
                        "userType",
                    ],
                },
            },
            id: { type: "number" },
            interval: { type: "number" },
            latitude: { type: "number" },
            longitude: { type: "number" },
            name: { type: "string" },
        },
        required: ["id", "interval", "latitude", "longitude", "name"],
    },
};

const datasourceEcoCounterMeasurementsJsonSchema = {
    type: "array",
    items: {
        type: "object",
        properties: {
            counts: { type: ["number", "null"] },
            date: { type: "string" },
            status: { type: ["number", "null"] },
        },
        required: ["date"],
    },
};

// Locations
const outputLocationsSDMA: Sequelize.ModelAttributes<any> = {
    id: {
        primaryKey: true,
        type: Sequelize.STRING,
    },
    lat: Sequelize.DECIMAL,
    lng: Sequelize.DECIMAL,
    name: Sequelize.STRING,
    route: Sequelize.STRING,
    vendor: Sequelize.STRING,
    vendor_id: Sequelize.STRING,

    // Audit database fields
    create_batch_id: { type: Sequelize.BIGINT }, // ID of input batch
    created_at: { type: Sequelize.DATE }, // Time of insertion
    created_by: { type: Sequelize.STRING(150) }, // Author or name of Worker of inserion
    update_batch_id: { type: Sequelize.BIGINT }, // ID last input batch which caused update
    updated_at: { type: Sequelize.DATE }, // Time of update
    updated_by: { type: Sequelize.STRING(150) }, // Author or name of Worker of update
};

// only for Validator

const outputLocationsJsonSchema = {
    type: "array",
    items: {
        type: "object",
        properties: {
            id: { type: "string" },
            lat: { type: "number" },
            lng: { type: "number" },
            name: { type: "string" },
            route: { type: ["string", "null"] },
            vendor_id: { type: ["string", "number"] },
        },
        required: ["id", "lat", "lng", "vendor_id"],
    },
};

// Directions
const outputDirectionsSDMA: Sequelize.ModelAttributes<any> = {
    id: {
        primaryKey: true,
        type: Sequelize.STRING,
    },
    locations_id: Sequelize.STRING,
    name: Sequelize.STRING,
    vendor_id: Sequelize.STRING,

    // Audit database fields
    create_batch_id: { type: Sequelize.BIGINT }, // ID of input batch
    created_at: { type: Sequelize.DATE }, // Time of insertion
    created_by: { type: Sequelize.STRING(150) }, // Author or name of Worker of inserion
    update_batch_id: { type: Sequelize.BIGINT }, // ID last input batch which caused update
    updated_at: { type: Sequelize.DATE }, // Time of update
    updated_by: { type: Sequelize.STRING(150) }, // Author or name of Worker of update
};

// only for Validator

const outputDirectionsJsonSchema = {
    type: "array",
    items: {
        type: "object",
        properties: {
            id: { type: "string" },
            locations_id: { type: "string" },
            name: { type: "string" },
            vendor_id: { type: ["string", "number"] },
        },
        required: ["id", "locations_id", "vendor_id"],
    },
};

// Detections
const outputDetectionsSDMA: Sequelize.ModelAttributes<any> = {
    category: { type: Sequelize.STRING(100) },
    directions_id: {
        primaryKey: true,
        type: Sequelize.STRING,
    },
    locations_id: {
        primaryKey: true,
        type: Sequelize.STRING,
    },
    measured_from: {
        primaryKey: true,
        type: Sequelize.DATE,
    },
    measured_to: Sequelize.DATE,
    value: Sequelize.INTEGER,

    // Audit database fields
    create_batch_id: { type: Sequelize.BIGINT }, // ID of input batch
    created_at: { type: Sequelize.DATE }, // Time of insertion
    created_by: { type: Sequelize.STRING(150) }, // Author or name of Worker of inserion
    update_batch_id: { type: Sequelize.BIGINT }, // ID last input batch which caused update
    updated_at: { type: Sequelize.DATE }, // Time of update
    updated_by: { type: Sequelize.STRING(150) }, // Author or name of Worker of update
};

// only for Validator

const outputDetectionsJsonSchema = {
    type: "array",
    items: {
        type: "object",
        properties: {
            directions_id: { type: "string" },
            locations_id: { type: "string" },
            measured_from: { type: "object", required: ["toISOString"] },
            measured_to: { type: "object", required: ["toISOString"] },
            value: { type: ["number", "null"] },
        },
        required: ["directions_id", "locations_id", "measured_from", "measured_to"],
    },
};

const forExportCounters = {
    pgSchema: "flow",
    detections: {
        name: "CountersDetections",
        outputJsonSchemaObject: outputDetectionsJsonSchema,
        outputSequelizeAttributes: outputDetectionsSDMA,
        pgTableName: "counters_detections",
    },
    directions: {
        name: "CountersDirections",
        outputJsonSchemaObject: outputDirectionsJsonSchema,
        outputSequelizeAttributes: outputDirectionsSDMA,
        pgTableName: "counters_directions",
    },
    locations: {
        name: "CountersLocations",
        outputJsonSchemaObject: outputLocationsJsonSchema,
        outputSequelizeAttributes: outputLocationsSDMA,
        pgTableName: "counters_locations",
    },
    ecoCounter: {
        datasourceJsonSchema: datasourceEcoCounterJsonSchema,
        measurementsDatasourceJsonSchema: datasourceEcoCounterMeasurementsJsonSchema,
        name: "EcoCounterCounters",
    },
};

export { forExportCounters as Counters };
export { forExport as Flow };
