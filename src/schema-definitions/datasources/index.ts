import { cubesDatasource } from "./CubesDatasource";
import { analyticsDatasource } from "./AnalyticsDatasource";
import { sinksDatasource } from "./SinksDatasource";
import { sinksHistoryDatasource } from "./SinksHistoryDatasource";

export { cubesDatasource, analyticsDatasource, sinksDatasource, sinksHistoryDatasource };
