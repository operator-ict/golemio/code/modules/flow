import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";

export interface ICubesInput {
    id: number;
    name: string;
}

const cubesJsonSchema: JSONSchemaType<ICubesInput[]> = {
    type: "array",
    items: {
        type: "object",
        properties: {
            id: { type: "number" },
            name: { type: "string" },
        },
        required: ["id", "name"],
        additionalProperties: false,
    },
};

export const cubesDatasource: { name: string; jsonSchema: JSONSchemaType<ICubesInput[]> } = {
    name: "CubesDatasource",
    jsonSchema: cubesJsonSchema,
};
