import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";

export interface IAnalyticsInput {
    id: number;
}

const analyticsJsonSchema: JSONSchemaType<IAnalyticsInput[]> = {
    type: "array",
    items: {
        type: "object",
        properties: {
            id: { type: "number" },
        },
        required: ["id"],
        additionalProperties: false,
    },
};

export const analyticsDatasource: { name: string; jsonSchema: JSONSchemaType<IAnalyticsInput[]> } = {
    name: "AnalyticsDatasource",
    jsonSchema: analyticsJsonSchema,
};
