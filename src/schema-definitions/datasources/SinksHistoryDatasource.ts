import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";

interface ISinksHistorySnapshotCategory {
    category: string;
    count: number;
}

interface ISinksHistorySnapshotDirection {
    name: string;
    id: number;
}

interface ISinksHistorySnapshotTurningMovement {
    category: string;
    data: number[][];
}

interface ISinksHistorySnapshot {
    data: {
        categories?: ISinksHistorySnapshotCategory[];
        destinations?: ISinksHistorySnapshotDirection[];
        origins?: ISinksHistorySnapshotDirection[];
        turning_movements?: ISinksHistorySnapshotTurningMovement[];

        data_validity: string;
        object_count: number;
    };
    data_end_timestamp: string;
    data_start_timestamp: string;
    timestamp: string;
}

export interface ISinksHistoryInput {
    history: {
        enabled: boolean;
        policy: string;
    };
    history_capacity: number;
    history_records_count: number;
    history_start_timestamp: string;
    id: number;
    name: string;
    output_type: string;
    output_value_type: string;
    snapshots: ISinksHistorySnapshot[];
}

const sinksHistoryJsonSchema: JSONSchemaType<ISinksHistoryInput[]> = {
    type: "array",
    items: {
        type: "object",
        properties: {
            history: {
                type: "object",
                properties: {
                    enabled: { type: "boolean" },
                    policy: { type: "string" },
                },
                required: ["enabled", "policy"],
            },
            history_capacity: { type: "number" },
            history_records_count: { type: "number" },
            history_start_timestamp: { type: "string" },
            id: { type: "number" },
            name: { type: "string" },
            output_type: { type: "string" },
            output_value_type: { type: "string" },
            snapshots: {
                type: "array",
                items: {
                    type: "object",
                    properties: {
                        data: {
                            type: "object",
                            properties: {
                                categories: {
                                    type: "array",
                                    items: {
                                        type: "object",
                                        properties: {
                                            category: { type: "string" },
                                            count: { type: "number" },
                                        },
                                        required: ["category", "count"],
                                    },
                                    nullable: true,
                                },
                                destinations: {
                                    type: "array",
                                    items: {
                                        type: "object",
                                        properties: {
                                            name: { type: "string" },
                                            id: { type: "number" },
                                        },
                                        required: ["name", "id"],
                                    },
                                    nullable: true,
                                },
                                origins: {
                                    type: "array",
                                    items: {
                                        type: "object",
                                        properties: {
                                            name: { type: "string" },
                                            id: { type: "number" },
                                        },
                                        required: ["name", "id"],
                                    },
                                    nullable: true,
                                },
                                turning_movements: {
                                    type: "array",
                                    items: {
                                        type: "object",
                                        properties: {
                                            category: { type: "string" },
                                            data: {
                                                type: "array",
                                                items: { type: "array", items: { type: "number" } },
                                            },
                                        },
                                        required: ["category", "data"],
                                    },
                                    nullable: true,
                                },
                                data_validity: { type: "string" },
                                object_count: { type: "number" },
                            },
                            required: ["data_validity", "object_count"],
                        },
                        data_end_timestamp: { type: "string" },
                        data_start_timestamp: { type: "string" },
                        timestamp: { type: "string" },
                    },
                    required: ["data", "data_end_timestamp", "data_start_timestamp", "timestamp"],
                },
            },
        },
        required: [
            "history",
            "history_capacity",
            "history_records_count",
            "history_start_timestamp",
            "id",
            "name",
            "output_value_type",
            "output_type",
            "snapshots",
        ],
        additionalProperties: false,
    },
};

export const sinksHistoryDatasource: { name: string; jsonSchema: JSONSchemaType<ISinksHistoryInput[]> } = {
    name: "SinksHistoryDatasource",
    jsonSchema: sinksHistoryJsonSchema,
};
