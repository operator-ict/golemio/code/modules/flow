import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";

export interface ISinksInput {
    history: {
        enabled: boolean;
        policy: string;
    };
    history_capacity: number;
    history_records_count: number;
    history_start_timestamp: string;
    id: number;
    name: string;
    output_value_type: string;
}

const sinksJsonSchema: JSONSchemaType<ISinksInput[]> = {
    type: "array",
    items: {
        type: "object",
        properties: {
            history: {
                type: "object",
                properties: {
                    enabled: { type: "boolean" },
                    policy: { type: "string" },
                },
                required: ["enabled", "policy"],
            },
            history_capacity: { type: "number" },
            history_records_count: { type: "number" },
            history_start_timestamp: { type: "string" },
            id: { type: "number" },
            name: { type: "string" },
            output_value_type: { type: "string" },
        },
        required: [
            "history",
            "history_capacity",
            "history_records_count",
            "history_start_timestamp",
            "id",
            "name",
            "output_value_type",
        ],
        additionalProperties: false,
    },
};

export const sinksDatasource: { name: string; jsonSchema: JSONSchemaType<ISinksInput[]> } = {
    name: "SinksDatasource",
    jsonSchema: sinksJsonSchema,
};
