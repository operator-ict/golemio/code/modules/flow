import { Counters } from "#sch";
import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine/transformations";
import { dateTime } from "@golemio/core/dist/helpers/DateTime";

export class EcoCounterMeasurementsTransformation extends BaseTransformation implements ITransformation {
    public name: string;

    constructor() {
        super();
        this.name = Counters.ecoCounter.name + "MeasurementsTransformation";
    }

    protected transformElement = async (element: any): Promise<any> => {
        // Repair UTC date, because EcoCounter API is actually working with local Europe/Prague time, not ISO!!!
        // Returned value 07:00:00+0000 is some hybrid between UTC time with offset 07:00:00+0100 and pure UTC
        // 06:00:00+0000

        const measuredFrom = dateTime(new Date(element.date.split("+")[0])).setTimeZone("Europe/Prague");

        const measuredTo = measuredFrom.clone().add(15, "minutes");

        const res = {
            directions_id: null,
            locations_id: null,
            measured_from: measuredFrom.toDate(),
            measured_to: measuredTo.toDate(),
            value: element.counts,
        };

        return res;
    };
}
