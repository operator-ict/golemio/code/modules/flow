export interface IGetSinksHistoryPayloads {
    analytic: {
        id: number;
        name: string;
        sequence_number: string;
    };
    cube: {
        id: number;
        name: string;
        historyIntervalsArr: number[][];
    };
    sinks: {
        id: string;
        history: {
            enabled: boolean;
            policy: string;
        };
    };
    sinksOutputValueType: string;
    historyIntervalsArr: number[][];
}
