export interface IHistoryIntervalMessage {
    from: string;
    to: string;
}
