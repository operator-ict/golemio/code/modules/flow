export interface IGetSinksMessage {
    analytic: {
        id: number;
        name: string;
        sequence_number: string;
    };
    cube: {
        id: number;
        name: string;
        historyIntervalsArr: number[][];
    };
    historyIntervalsArr: number[][];
}
