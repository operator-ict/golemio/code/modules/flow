export interface IGetAnalyticsMessage {
    id: number;
    name: string;
    historyIntervalsArr: number[][];
}
