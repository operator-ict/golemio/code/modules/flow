export interface IGetSinksHistoryMessage {
    analytic: {
        id: number;
        name: string;
        sequence_number: string;
    };
    cube: {
        id: number;
        name: string;
        historyIntervalsArr: number[][];
    };
    payload: {
        sequence_number: string;
        sinks: Array<{
            id: number;
            start_timestamp: string;
            end_timestamp: string;
        }>;
    };
    sinksOutputValueType: string;
}
