import { Counters } from "#sch/index";
import { dateTime } from "@golemio/core/dist/helpers/DateTime";
import { config } from "@golemio/core/dist/integration-engine/config";
import { DataSource, JSONDataTypeStrategy } from "@golemio/core/dist/integration-engine/datasources";
import { HTTPFetchProtocolStrategy } from "@golemio/core/dist/integration-engine/datasources/protocol-strategy/HTTPFetchProtocolStrategy";
import { PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { BaseWorker } from "@golemio/core/dist/integration-engine/workers";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { EcoCounterMeasurementsTransformation } from "./transformations/EcoCounterMeasurementsTransformation";
import { EcoCounterTransformation } from "./transformations/EcoCounterTransformation";

export class CountersWorker extends BaseWorker {
    private dataSourceEcoCounter: DataSource;
    private dataSourceEcoCounterMeasurements: DataSource;

    private ecoCounterTransformation: EcoCounterTransformation;
    private ecoCounterMeasurementsTransformation: EcoCounterMeasurementsTransformation;

    private queuePrefix: string;

    private countersLocationsModel: PostgresModel;
    private countersDirectionsModel: PostgresModel;
    private countersDetectionsModel: PostgresModel;

    constructor() {
        super();

        const parser = new JSONDataTypeStrategy({ resultsPath: "" });
        parser.setFilter((item) => item.counter !== null);
        this.dataSourceEcoCounter = new DataSource(
            Counters.ecoCounter.name + "DataSource",
            new HTTPFetchProtocolStrategy({
                headers: {
                    Authorization: `Bearer ${config.datasources.CountersEcoCounterTokens.OICT}`,
                },
                method: "GET",
                url: config.datasources.BicycleCountersEcoCounter,
            }),
            parser,
            new JSONSchemaValidator(Counters.ecoCounter.name + "Validator", Counters.ecoCounter.datasourceJsonSchema)
        );
        this.dataSourceEcoCounterMeasurements = new DataSource(
            Counters.ecoCounter.name + "MeasurementsDataSource",
            undefined as any,
            new JSONDataTypeStrategy({ resultsPath: "" }),
            new JSONSchemaValidator(
                Counters.ecoCounter.name + "MeasurementsDataSource",
                Counters.ecoCounter.measurementsDatasourceJsonSchema
            )
        );
        this.ecoCounterTransformation = new EcoCounterTransformation();
        this.ecoCounterMeasurementsTransformation = new EcoCounterMeasurementsTransformation();

        this.countersLocationsModel = new PostgresModel(
            Counters.locations.name + "Model",
            {
                outputSequelizeAttributes: Counters.locations.outputSequelizeAttributes,
                pgTableName: Counters.locations.pgTableName,
                pgSchema: Counters.pgSchema,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(Counters.locations.name + "ModelValidator", Counters.locations.outputJsonSchemaObject)
        );
        this.countersDirectionsModel = new PostgresModel(
            Counters.directions.name + "Model",
            {
                outputSequelizeAttributes: Counters.directions.outputSequelizeAttributes,
                pgTableName: Counters.directions.pgTableName,
                pgSchema: Counters.pgSchema,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(Counters.directions.name + "ModelValidator", Counters.directions.outputJsonSchemaObject)
        );
        this.countersDetectionsModel = new PostgresModel(
            Counters.detections.name + "Model",
            {
                outputSequelizeAttributes: Counters.detections.outputSequelizeAttributes,
                pgTableName: Counters.detections.pgTableName,
                pgSchema: Counters.pgSchema,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(Counters.detections.name + "ModelValidator", Counters.detections.outputJsonSchemaObject)
        );

        this.queuePrefix = config.RABBIT_EXCHANGE_NAME + "." + "counters";
    }

    public refreshEcoCounterDataInDB = async (msg: any): Promise<void> => {
        const data = await this.dataSourceEcoCounter.getAll();

        const transformedData = await this.ecoCounterTransformation.transform(data);

        await this.countersLocationsModel.save(transformedData.locationsPedestrians);
        await this.countersDirectionsModel.save(transformedData.directionsPedestrians);

        const promisesPeds = transformedData.directionsPedestrians.map((p) => {
            this.sendMessageToExchange(
                "workers." + this.queuePrefix + ".updateEcoCounter",
                JSON.stringify({
                    category: "pedestrian",
                    directions_id: p.id,
                    id: p.vendor_id,
                    locations_id: p.locations_id,
                })
            );
        });
        await Promise.all(promisesPeds);
    };

    public updateEcoCounter = async (msg: any): Promise<void> => {
        const inputData = JSON.parse(msg.content.toString());
        const locationsId = inputData.locations_id;
        const directionsId = inputData.directions_id;
        const category = inputData.category;
        const id = inputData.id;

        // EcoCounter API is actually working with local Europe/Prague time, not ISO!!!
        // so we have to send local time to request.
        // Furthermore, the returned dates are START of the measurement interval, so if we want measurements
        // from interval between 06:00 and 07:00 UTC (which is local 07:00 - 08:00), we have to send parameters
        // from=07:00 and to=07:45, because it returns all the measurements where from and to parameters are INCLUDED.

        const now = new Date().setUTCSeconds(0, 0);

        const step = 15;

        const remainder2 = new Date(now).getMinutes() % step;

        const nowRound = dateTime(new Date(now), { timeZone: "UTC" })
            .setTimeZone("Europe/Prague")
            .subtract(remainder2, "minutes");

        const strTo = nowRound.clone().subtract(step, "minutes").format("yyyy-LL-dd'T'HH:mm:ss");

        const strFrom = nowRound.clone().subtract(72, "hours").format("yyyy-LL-dd'T'HH:mm:ss");

        let url = config.datasources.BicycleCountersEcoCounterMeasurements;
        url = url.replace(":id", id);
        url = url.replace(":from", strFrom);
        url = url.replace(":to", strTo);
        url = url.replace(":step", `${step}m`);
        url = url.replace(":complete", "true");

        this.dataSourceEcoCounterMeasurements.setProtocolStrategy(
            new HTTPFetchProtocolStrategy({
                headers: {
                    Authorization: `Bearer ${config.datasources.CountersEcoCounterTokens.OICT}`,
                },
                method: "GET",
                url,
            })
        );

        const data = await this.dataSourceEcoCounterMeasurements.getAll();

        // pedestrians
        if (category === "pedestrian") {
            await this.countersDetectionsModel.saveBySqlFunction(
                (
                    await this.ecoCounterMeasurementsTransformation.transform(data)
                ).map((x: any) => {
                    x.directions_id = directionsId;
                    x.locations_id = locationsId;
                    x.category = "pedestrian";
                    return x;
                }),
                ["locations_id", "directions_id", "measured_from", "category"]
            );
        }
    };
}
