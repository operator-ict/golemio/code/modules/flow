import { analyticsDatasource, cubesDatasource, sinksDatasource, sinksHistoryDatasource } from "#sch/datasources";
import { Flow } from "#sch/index";
import { dateTime } from "@golemio/core/dist/helpers/DateTime";
import { config } from "@golemio/core/dist/integration-engine/config";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import {
    DataSourceStream,
    DataSourceStreamed,
    IHTTPSettings,
    JSONDataTypeStrategy,
} from "@golemio/core/dist/integration-engine/datasources";
import { HTTPRequestProtocolStrategyStreamed } from "@golemio/core/dist/integration-engine/datasources/protocol-strategy/HTTPRequestProtocolStrategyStreamed";
import { log } from "@golemio/core/dist/integration-engine/helpers";
import { PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { BaseWorker } from "@golemio/core/dist/integration-engine/workers";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import Sequelize from "@golemio/core/dist/shared/sequelize";
import { Client, Dispatcher } from "@golemio/core/dist/shared/undici";
import JSONStream from "JSONStream";
import https from "https";
import { IHistoryIntervalMessage } from "./interfaces/IHistoryIntervalMessage";
import { IGetAnalyticsMessage } from "./interfaces/IGetAnalyticsMessage";
import { IGetSinksMessage } from "./interfaces/IGetSinksMessage";
import { IGetSinksHistoryPayloads } from "./interfaces/IGetSinksHistoryPayloads";
import { IGetSinksHistoryMessage } from "./interfaces/IGetSinksHistoryMessage";

export class FlowWorker extends BaseWorker {
    private cubesDataSource: DataSourceStreamed;
    private analyticsDataSource: DataSourceStreamed;
    private sinksDataSource: DataSourceStreamed;
    private sinksHistoryDataSource: DataSourceStreamed;

    private flowCubesModel: PostgresModel;
    private flowMeasurementModel: PostgresModel;
    private flowODMeasurementModel: PostgresModel;
    private flowSinksModel: PostgresModel;

    private queuePrefix: string = `${config.RABBIT_EXCHANGE_NAME}.${Flow.detections.name.toLowerCase()}`;

    constructor() {
        super();

        this.flowMeasurementModel = new PostgresModel(
            Flow.detections.name + "Model",
            {
                outputSequelizeAttributes: Flow.detections.outputSequelizeAttributes,
                pgSchema: Flow.pgSchema,
                pgTableName: Flow.detections.pgTableName,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator("FlowMeasurementValidator", Flow.detections.jsonSchema)
        );

        this.flowODMeasurementModel = new PostgresModel(
            Flow.detectionsOD.name + "Model",
            {
                outputSequelizeAttributes: Flow.detectionsOD.outputSequelizeAttributes,
                pgSchema: Flow.pgSchema,
                pgTableName: Flow.detectionsOD.pgTableName,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator("FlowODMeasurementValidator", Flow.detectionsOD.jsonSchema)
        );

        this.flowCubesModel = new PostgresModel(
            Flow.cubes.name + "Model",
            {
                outputSequelizeAttributes: Flow.cubes.outputSequelizeAttributes,
                pgSchema: Flow.pgSchema,
                pgTableName: Flow.cubes.pgTableName,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator("FlowCubesValidator", Flow.cubes.jsonSchema)
        );

        this.flowSinksModel = new PostgresModel(
            Flow.sinks.name + "Model",
            {
                outputSequelizeAttributes: Flow.sinks.outputSequelizeAttributes,
                pgSchema: Flow.pgSchema,
                pgTableName: Flow.sinks.pgTableName,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator("FlowSinksValidator", Flow.sinks.jsonSchema)
        );

        this.cubesDataSource = new DataSourceStreamed(
            cubesDatasource.name,
            new HTTPRequestProtocolStrategyStreamed({
                headers: {},
                method: "GET",
                url: config.datasources.FlowAPI,
            }).setStreamTransformer(JSONStream.parse("cubes.*")),
            new JSONDataTypeStrategy({ resultsPath: "" }),
            new JSONSchemaValidator(cubesDatasource.name + "Validator", cubesDatasource.jsonSchema)
        );

        this.analyticsDataSource = new DataSourceStreamed(
            analyticsDatasource.name,
            new HTTPRequestProtocolStrategyStreamed({
                headers: {},
                method: "GET",
                url: config.datasources.FlowAPI,
            }).setStreamTransformer(JSONStream.parse("analytics.*")),
            new JSONDataTypeStrategy({ resultsPath: "" }),
            new JSONSchemaValidator(analyticsDatasource.name + "Validator", analyticsDatasource.jsonSchema)
        );

        this.sinksDataSource = new DataSourceStreamed(
            sinksDatasource.name,
            new HTTPRequestProtocolStrategyStreamed({
                headers: {},
                method: "GET",
                url: config.datasources.FlowAPI,
            }).setStreamTransformer(JSONStream.parse("sinks.*")),
            new JSONDataTypeStrategy({ resultsPath: "" }),
            new JSONSchemaValidator(sinksDatasource.name + "Validator", sinksDatasource.jsonSchema)
        );

        this.sinksHistoryDataSource = new DataSourceStreamed(
            sinksHistoryDatasource.name,
            new HTTPRequestProtocolStrategyStreamed({
                headers: {},
                method: "POST",
                url: config.datasources.FlowAPI,
            }).setStreamTransformer(JSONStream.parse("sinks.*")),
            new JSONDataTypeStrategy({ resultsPath: "" }),
            new JSONSchemaValidator(sinksHistoryDatasource.name + "Validator", sinksHistoryDatasource.jsonSchema)
        );
    }

    public refreshMatViewPedestriansDetectApi = async (): Promise<any> => {
        try {
            const connection = PostgresConnector.getConnection();
            await connection.query(`CALL "${Flow.pgSchema}".update_detections_data()`, {
                type: Sequelize.QueryTypes.SELECT,
            });
        } catch (err) {
            throw new GeneralError("Error while aggregating flow data.", this.constructor.name, err);
        }
    };

    private getHistoryIntervalFromMsg = (msg: any): number[][] | null => {
        let historyIntervalsArr: number[][] = [];
        let msgObj = {};

        try {
            msgObj = JSON.parse(msg.content.toString());
        } catch (err) {
            return null;
        }

        const customIntervalArr: IHistoryIntervalMessage[] = !Array.isArray(msgObj) ? [msgObj] : msgObj;

        customIntervalArr.forEach((intervalObj) => {
            if (intervalObj.from && intervalObj.to) {
                const from = +new Date(intervalObj.from);
                const to = +new Date(intervalObj.to);

                // if range is more than 12hrs:
                if (to - from > 43200000) {
                    throw new GeneralError(
                        "Maximum interval size exceeded. Use intervals of up to 12 hours.",
                        this.constructor.name
                    );
                }

                log.debug(`History interval from: ${from} to ${to} was used.`);
                historyIntervalsArr.push([from, to]);
            } else {
                throw new GeneralError("Interval must contain from and to properties.", this.constructor.name);
            }
        });

        return historyIntervalsArr;
    };

    public refreshCubes = async (msg: any): Promise<void> => {
        let historyIntervalsArr = this.getHistoryIntervalFromMsg(msg);

        let dataStream: DataSourceStream;

        try {
            this.cubesDataSource.protocolStrategy.setConnectionSettings(this.getHttpSettings("", "GET", null));
            dataStream = await this.cubesDataSource.getAll(true);
        } catch (err) {
            throw new GeneralError("Error while getting data.", this.constructor.name, err);
        }

        try {
            await dataStream
                .setDataProcessor(async (data: any) => {
                    await this.flowCubesModel.save(
                        data.map((cube: any) => {
                            return {
                                id: cube.id,
                                name: cube.name,
                            };
                        })
                    );
                    const promises = data.map((cube: any) => {
                        cube.historyIntervalsArr = historyIntervalsArr;
                        return this.sendMessageToExchange("workers." + this.queuePrefix + ".getAnalytics", JSON.stringify(cube));
                    });
                    await Promise.all(promises);
                })
                .proceed();
        } catch (err) {
            throw new GeneralError("Error while processing data.", this.constructor.name, err);
        }
    };

    public getAnalytics = async (msg: any): Promise<void> => {
        let input: IGetAnalyticsMessage;
        let dataStream: DataSourceStream;
        let historyIntervalsArr: number[][] | null;
        try {
            input = JSON.parse(msg.content.toString());

            if (input?.id === undefined) {
                throw new Error(`Can not process getAnalytics input: ${input}`);
            }
            historyIntervalsArr = input.historyIntervalsArr;

            this.analyticsDataSource.protocolStrategy.setConnectionSettings(
                this.getHttpSettings(`/${input.id}/analytics`, "GET", null)
            );
            dataStream = await this.analyticsDataSource.getAll(true);
        } catch (err) {
            throw new GeneralError("Error while getting data.", this.constructor.name, err);
        }

        try {
            await dataStream
                .setDataProcessor(async (data: any) => {
                    const url = new URL(config.datasources.FlowAPI.url);
                    const clientOptions: Client.Options = {
                        connect: {
                            rejectUnauthorized: false,
                        },
                    };
                    const client = new Client(url.origin, clientOptions);
                    const httpsAgent = new https.Agent({
                        rejectUnauthorized: false,
                    });

                    const axPromises = data.map(async (analytic: any) => {
                        // no need for DataSource
                        const fullUrl = new URL(`${config.datasources.FlowAPI.url}/${input.id}/analytics/${analytic.id}`);
                        const requestOptions: Dispatcher.RequestOptions = {
                            path: fullUrl.pathname + fullUrl.search,
                            method: "GET" as Dispatcher.HttpMethod,
                            headers: {
                                "Accept-Version": "1.3",
                                Authorization: `Bearer ${config.datasources.FlowAPI.token}`,
                                "Content-Type": "application/json",
                            },
                        };
                        const result = await client.request(requestOptions);

                        return result.body.json();
                    });

                    const analytics = await Promise.all(axPromises);

                    const exPromises = analytics.map((analytic: any) => {
                        return this.sendMessageToExchange(
                            "workers." + this.queuePrefix + ".getSinks",
                            JSON.stringify({
                                analytic: analytic,
                                cube: input,
                                historyIntervalsArr,
                            })
                        );
                    });
                    await Promise.all(exPromises);
                })
                .proceed();
        } catch (err) {
            throw new GeneralError("Error while processing data.", this.constructor.name, err);
        }
    };

    public getSinks = async (msg: any): Promise<void> => {
        let input: IGetSinksMessage;
        let dataStream: DataSourceStream;
        let historyIntervalsArr: number[][] | null;
        try {
            input = JSON.parse(msg.content.toString());

            if (input?.analytic?.id === undefined || input?.cube?.id === undefined) {
                throw new Error(`Can not process getAnalytics input: ${JSON.stringify(input)}`);
            }
            historyIntervalsArr = input.historyIntervalsArr;

            this.sinksDataSource.protocolStrategy.setConnectionSettings(
                this.getHttpSettings(`/${input.cube.id}/analytics/${input.analytic.id}/sinks`, "GET", null)
            );

            dataStream = await this.sinksDataSource.getAll(true);
        } catch (err) {
            throw new GeneralError("Error while getting data.", this.constructor.name, err);
        }

        try {
            await dataStream
                .setDataProcessor(async (data: any) => {
                    await this.flowSinksModel.save(
                        data.map((sink: Record<string, any>) => {
                            return {
                                cube_id: +input.cube.id,
                                history_start_timestamp: new Date(+sink.history_start_timestamp),
                                id: sink.id,
                                name: sink.name,
                                output_value_type: sink.output_value_type,
                            };
                        })
                    );

                    const dataWithDistributionSinks = data.filter((sink: any) => {
                        return sink?.output_value_type === "distribution";
                    });

                    const dataWithOdMatrixSinks = data.filter((sink: any) => {
                        return sink?.output_value_type === "od_matrix";
                    });

                    if (dataWithDistributionSinks.length)
                        await this.sendMessageToExchange(
                            "workers." + this.queuePrefix + ".getSinksHistoryPayloads",
                            JSON.stringify({
                                analytic: input.analytic,
                                cube: input.cube,
                                sinks: dataWithDistributionSinks,
                                sinksOutputValueType: "distribution",
                                historyIntervalsArr,
                            })
                        );

                    if (dataWithOdMatrixSinks.length)
                        await this.sendMessageToExchange(
                            "workers." + this.queuePrefix + ".getSinksHistoryPayloads",
                            JSON.stringify({
                                analytic: input.analytic,
                                cube: input.cube,
                                sinks: dataWithOdMatrixSinks,
                                sinksOutputValueType: "od_matrix",
                                historyIntervalsArr,
                            })
                        );
                })
                .proceed();
        } catch (err) {
            throw new GeneralError("Error while processing data.", this.constructor.name, err);
        }
    };

    public getSinksHistoryPayloads = async (msg: any): Promise<void> => {
        let input: IGetSinksHistoryPayloads;
        let historyIntervalsArr: number[][] | null;
        let timeRanges: any[];
        try {
            input = JSON.parse(msg.content.toString());

            if (
                input?.analytic?.id === undefined ||
                input?.cube?.id === undefined ||
                !input.sinks ||
                !input.sinksOutputValueType
            ) {
                throw new Error(`Can not process getAnalytics input: ${JSON.stringify(input)}`);
            }
            historyIntervalsArr = input.historyIntervalsArr;

            if (historyIntervalsArr) timeRanges = historyIntervalsArr;
            else timeRanges = this.getTimeRanges();

            await Promise.all(
                this.getHistoryPayload(timeRanges, input).map((payload: any) => {
                    return this.sendMessageToExchange(
                        "workers." + this.queuePrefix + ".getSinksHistory",
                        JSON.stringify({
                            analytic: input.analytic,
                            cube: input.cube,
                            payload,
                            sinksOutputValueType: input.sinksOutputValueType,
                        })
                    );
                })
            );
        } catch (err) {
            throw new GeneralError("Error while getting data.", this.constructor.name, err);
        }
    };

    public getSinksHistory = async (msg: any): Promise<void> => {
        let dataStream: DataSourceStream;
        let input: IGetSinksHistoryMessage;

        try {
            input = JSON.parse(msg.content.toString());

            if (
                input?.analytic?.id === undefined ||
                input?.cube?.id === undefined ||
                !input.payload ||
                !input.sinksOutputValueType
            ) {
                throw new Error(`Can not process getAnalytics input: ${JSON.stringify(input)}`);
            }

            this.sinksHistoryDataSource.protocolStrategy.setConnectionSettings(
                this.getHttpSettings(`/${input.cube.id}/analytics/${input.analytic.id}/sinks/history`, "POST", input.payload)
            );

            dataStream = await this.sinksHistoryDataSource.getAll(true);
        } catch (err) {
            throw new GeneralError("Error while getting data.", this.constructor.name, err);
        }

        if (input.sinksOutputValueType === "distribution")
            try {
                await dataStream
                    .setDataProcessor(async (data: any[]) => {
                        const detections = this.getMeasurementsDistribution(
                            data,
                            input.cube.id,
                            input.payload.sequence_number,
                            input.analytic.id
                        );
                        // filter duplicate data
                        const uniq = detections.filter(
                            (thing: Record<string, any>, index: number, self: any[]) =>
                                index ===
                                self.findIndex(
                                    (t) =>
                                        t.cube_id === thing.cube_id &&
                                        t.sink_id === thing.sink_id &&
                                        t.start_timestamp === thing.start_timestamp &&
                                        t.end_timestamp === thing.end_timestamp &&
                                        t.category === thing.category &&
                                        t.sequence_number === thing.sequence_number &&
                                        t.data_validity === thing.data_validity
                                )
                        );

                        await this.flowMeasurementModel.save(uniq, false);
                    })
                    .proceed();
            } catch (err) {
                throw new GeneralError("Error while processing data (distribution sinks).", this.constructor.name, err);
            }

        if (input.sinksOutputValueType === "od_matrix")
            try {
                await dataStream
                    .setDataProcessor(async (data: any[]) => {
                        const detections = this.getMeasurementsOdMatrix(
                            data,
                            input.cube.id,
                            input.payload.sequence_number,
                            input.analytic.id
                        );

                        // filter duplicate data
                        const uniq = detections.filter(
                            (thing: Record<string, any>, index: number, self: any[]) =>
                                index ===
                                self.findIndex(
                                    (t) =>
                                        t.cube_id === thing.cube_id &&
                                        t.sink_id === thing.sink_id &&
                                        t.start_timestamp === thing.start_timestamp &&
                                        t.end_timestamp === thing.end_timestamp &&
                                        t.category === thing.category &&
                                        t.sequence_number === thing.sequence_number &&
                                        t.data_validity === thing.data_validity &&
                                        t.origin === thing.origin &&
                                        t.destination === thing.destination
                                )
                        );
                        await this.flowODMeasurementModel.save(uniq, false);
                    })
                    .proceed();
            } catch (err) {
                throw new GeneralError("Error while processing data (od_matrix sinks).", this.constructor.name, err);
            }
    };

    private getMeasurementsDistribution = (data: any, cube_id: any, sequence_number: any, analytic_id: any): any => {
        const measurementsNormalised: any[] = [];

        data.forEach((element: any) => {
            (element?.snapshots || []).forEach((snapshot: any) => {
                (snapshot?.data?.categories || []).forEach((category: any) => {
                    // filter out 'old' format
                    const startTimestamp = parseInt(snapshot.data_start_timestamp, 10);
                    if (startTimestamp && !isNaN(startTimestamp)) {
                        measurementsNormalised.push({
                            sink_id: element.id,
                            cube_id,
                            sequence_number: +sequence_number,
                            analytic_id,
                            start_timestamp: new Date(+snapshot.data_start_timestamp),
                            end_timestamp: new Date(+snapshot.data_end_timestamp),
                            category: category.category,
                            value: category.count,
                            data_validity: snapshot.data.data_validity,
                        });
                    }
                });
            });
        });
        return measurementsNormalised;
    };

    private getMeasurementsOdMatrix = (data: any, cube_id: any, sequence_number: any, analytic_id: any): any => {
        const measurementsNormalised: any[] = [];

        data.forEach((element: any) => {
            (element?.snapshots || []).forEach((snapshot: any) => {
                const destinations: [{ id: number; name: string }] = snapshot.data.destinations;
                const origins: [{ id: number; name: string }] = snapshot.data.origins;

                (snapshot?.data?.turning_movements || []).forEach((category: any, categoryIndex: number) => {
                    const categoryName = category.category;

                    // filter out 'old' format
                    const startTimestamp = parseInt(snapshot.data_start_timestamp, 10);
                    if (startTimestamp && !isNaN(startTimestamp)) {
                        category.data.forEach((originRow: [], originIndex: number) => {
                            originRow.forEach((value, destinationIndex) => {
                                measurementsNormalised.push({
                                    sink_id: element.id,
                                    cube_id,
                                    sequence_number: +sequence_number,
                                    analytic_id,
                                    start_timestamp: new Date(+snapshot.data_start_timestamp),
                                    end_timestamp: new Date(+snapshot.data_end_timestamp),
                                    category: categoryName,
                                    origin: origins[originIndex].name,
                                    destination: destinations[destinationIndex].name,
                                    value,
                                    data_validity: snapshot.data.data_validity,
                                });
                            });
                        });
                    }
                });
            });
        });
        return measurementsNormalised;
    };

    private getHistoryPayload(timeRanges: any[], data: any): any {
        const payloads: any[] = [];

        timeRanges.forEach((range: number[]) => {
            const payload: Record<string, any> = {
                sequence_number: data.analytic.sequence_number,
                sinks: [],
            };
            data.sinks.forEach((sink: any) => {
                payload.sinks.push({
                    // has to be string!
                    end_timestamp: `${range[1]}`,
                    id: sink.id,
                    // has to be string!
                    start_timestamp: `${range[0]}`,
                });
            });
            payloads.push(payload);
        });
        return payloads;
    }

    // get array of all pastOffset to past timeranges [startTS, endTS]
    // where endTS - startTS === interval starting now - pastOffset
    private getTimeRanges = (pastOffset: number = 60, interval: number = 15): any[] => {
        const start = dateTime(new Date(new Date().setUTCSeconds(0, 0)));

        const fullMinsDiv2 = start.toDate().getMinutes() % interval;

        const startTs = start.subtract(fullMinsDiv2, "minutes").valueOf();

        const endTs = start.subtract(pastOffset, "minutes").valueOf();

        const ranges = [];

        let currentTs = startTs;
        const intervalMs = interval * 60 * 1000;

        while (currentTs - intervalMs > endTs) {
            ranges.push([currentTs - intervalMs, currentTs]);
            currentTs = currentTs - intervalMs;
        }

        return ranges;
    };

    private getHttpSettings = (suffix: string = "", method: string = "GET", body: any = null): IHTTPSettings => {
        const settings: any = {
            headers: {
                "Accept-Version": "1.3",
                Authorization: `Bearer ${config.datasources.FlowAPI.token}`,
                "Content-Type": "application/json",
            },
            method,
            rejectUnauthorized: false,
            url: `${config.datasources.FlowAPI.url}${suffix}`,
        };

        if (body) {
            settings.body = JSON.stringify(body);
        }

        return settings;
    };
}
