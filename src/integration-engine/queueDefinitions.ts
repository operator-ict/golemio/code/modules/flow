import { config } from "@golemio/core/dist/integration-engine/config";
import { IQueueDefinition } from "@golemio/core/dist/integration-engine/queueprocessors";
import { Flow } from "#sch/index";
import { FlowWorker } from "#ie/FlowWorker";
import { CountersWorker } from "./CountersWorker";

const queueDefinitions: IQueueDefinition[] = [
    {
        name: Flow.detections.name,
        queuePrefix: config.RABBIT_EXCHANGE_NAME + "." + Flow.detections.name.toLowerCase(),
        queues: [
            {
                name: "refreshCubes",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                    messageTtl: 23 * 60 * 60 * 1000, // 23 hours
                },
                worker: FlowWorker,
                workerMethod: "refreshCubes",
            },
            {
                name: "getAnalytics",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                    messageTtl: 23 * 60 * 60 * 1000, // 23 hours
                },
                worker: FlowWorker,
                workerMethod: "getAnalytics",
            },
            {
                name: "getSinks",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                    messageTtl: 23 * 60 * 60 * 1000, // 23 hours
                },
                worker: FlowWorker,
                workerMethod: "getSinks",
            },
            {
                name: "getSinksHistoryPayloads",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                    messageTtl: 23 * 60 * 60 * 1000, // 23 hours
                },
                worker: FlowWorker,
                workerMethod: "getSinksHistoryPayloads",
            },
            {
                name: "getSinksHistory",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                    messageTtl: 23 * 60 * 60 * 1000, // 23 hours
                },
                worker: FlowWorker,
                workerMethod: "getSinksHistory",
            },
            {
                name: "refreshMatViewPedestriansDetectApi",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                    messageTtl: 20 * 60 * 1000, // 20 min
                },
                worker: FlowWorker,
                workerMethod: "refreshMatViewPedestriansDetectApi",
            },
        ],
    },
    {
        name: "Counters",
        queuePrefix: config.RABBIT_EXCHANGE_NAME + "." + "counters",
        queues: [
            {
                name: "refreshEcoCounterDataInDB",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                    messageTtl: 23 * 60 * 60 * 1000, // 23 hours
                },
                worker: CountersWorker,
                workerMethod: "refreshEcoCounterDataInDB",
            },
            {
                name: "updateEcoCounter",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                    messageTtl: 23 * 60 * 60 * 1000, // 23 hours
                },
                worker: CountersWorker,
                workerMethod: "updateEcoCounter",
            },
        ],
    },
];

export { queueDefinitions };
