import Sequelize from "@golemio/core/dist/shared/sequelize";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { SequelizeModel } from "@golemio/core/dist/output-gateway/models";
import { Flow } from "#sch/index";

export class PedestriansMeasurementsModel extends SequelizeModel {
    public constructor() {
        super(
            Flow.pedestriansMeasurementsApi.name,
            Flow.pedestriansMeasurementsApi.pgTableName,
            Flow.pedestriansMeasurementsApi.outputSequelizeAttributes,
            {
                schema: Flow.pgSchema,
            }
        );
    }

    /**
     * @param {object} [options] Options object with params
     * @param {number} [options.limit] Limit
     * @param {number} [options.offset] Offset
     * @param {string} [options.locationId] Location id <br>
     * @param {string} [options.directionId] Direction id<br>
     * @param {string} [options.from] ISO date<br>
     * @param {string} [options.to] ISO date<br>
     * @returns Array of the retrieved records
     */
    public GetAll = async (
        options: {
            limit?: number;
            offset?: number;
            locationId?: string;
            directionId?: string;
            from?: string;
            to?: string;
        } = {}
    ): Promise<any> => {
        const { limit, offset, locationId, directionId, from, to } = options;
        try {
            // Request to pedestrians_detections_api
            const and: symbol = Sequelize.Op.and;
            const order: any[] = [];
            const where: any = {
                [and]: [],
            };

            if (locationId) {
                where[and].push({ location_id: locationId });
            }

            if (directionId) {
                where[and].push({ direction_id: directionId });
            }

            if (from) {
                where[and].push({
                    measured_from: {
                        [Sequelize.Op.gte]: from,
                    },
                });
            }

            if (to) {
                where[and].push({
                    measured_to: {
                        [Sequelize.Op.lte]: to,
                    },
                });
            }

            const attributes: string[] = ["measured_from", "measured_to", "location_id", "direction_id", "value", "quality"];

            order.push(["location_id", "DESC"]);

            return await this.sequelizeModel.findAll({
                attributes,
                limit,
                offset,
                order,
                raw: true,
                where,
            });
        } catch (err) {
            throw new GeneralError("Database error", "PedestriansMeasurementsModel", err, 500);
        }
    };

    public GetOne = async (id: number): Promise<object | null> => {
        return null;
    };
}
