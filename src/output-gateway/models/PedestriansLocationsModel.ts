import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { sequelizeConnection } from "@golemio/core/dist/output-gateway/database";
import Sequelize from "@golemio/core/dist/shared/sequelize";
import { SequelizeModel } from "@golemio/core/dist/output-gateway/models";
import { dateTime } from "@golemio/core/dist/helpers/DateTime";
import { Flow } from "#sch/index";

export interface IPedestriansDirectionsData {
    direction_id: string;
    cube_id: string;
    location_id: number;
    direction_name: string;
    direction_type: string;
}

export class PedestriansLocationsModel extends SequelizeModel {
    public constructor() {
        super(
            Flow.pedestriansLocationsApi.name,
            Flow.pedestriansLocationsApi.pgTableName,
            Flow.pedestriansLocationsApi.outputSequelizeAttributes,
            {
                schema: Flow.pgSchema,
            }
        );
    }

    /**
     * @param {object} [options] Options object with params
     * @param {number} [options.limit] Limit
     * @param {number} [options.offset] Offset
     * @param {string} [options.locationId] Location id <br>
     * @param {string} [options.locationName] Human readable location name<br>
     * @returns Array of the retrieved records
     */

    public GetAll = async (
        options: {
            limit?: number;
            offset?: number;
            locationId?: string;
            locationName?: string;
        } = {}
    ): Promise<any> => {
        const { limit, offset, locationId, locationName } = options;
        try {
            const and: symbol = Sequelize.Op.and;
            const order: any[] = [];
            const where: any = {
                [and]: [],
            };

            if (locationId) where[and].push({ location_id: locationId });

            if (locationName) where[and].push({ location_name: locationName });

            const attributes: string[] = [
                "location_id",
                "location_name",
                "lat",
                "lng",
                "address",
                "city_district",
                "tech",
                "map_url",
                "place_url",
                "measurement_start",
                "measurement_end",
            ];

            order.push(["location_id", "DESC"]);
            // Request to pedestrians_locations_api
            const pedestriansLocationsData = await this.sequelizeModel.findAll({
                attributes,
                limit,
                offset,
                order,
                raw: true,
                where,
            });

            // Request to pedestrians_directions_api

            let pedestriansDirectionsQuery = `
            SELECT *
            FROM ${Flow.pgSchema}.v_pedestrians_directions_api
            `;

            let pedestriansDirectionsFromDB: any[] = await sequelizeConnection?.query(pedestriansDirectionsQuery);

            const pedestriansDirectionsData: [IPedestriansDirectionsData] = (pedestriansDirectionsFromDB || [])[0];

            // datatransformation to responseArr

            let responseArr: any[] = [];

            pedestriansLocationsData.forEach((location) => {
                let directionsArr: any[] = [];
                pedestriansDirectionsData.forEach((direction) => {
                    if (direction.location_id == location.location_id) {
                        directionsArr.push({
                            id: direction.direction_id,
                            name: direction.direction_name,
                            to: +direction.direction_type,
                        });
                    }
                });
                responseArr.push({
                    id: "" + location.location_id,
                    location_name: location.location_name,
                    lat: location.lat,
                    lng: location.lng,
                    address: location.address,
                    city_district: location.city_district,
                    tech: location.tech,
                    map_url: location.place_url,
                    photo_url: location.map_url,
                    measurement_start: location.measurement_start
                        ? dateTime(location.measurement_start).format("yyyy-LL-dd HH:mm")
                        : null,
                    measurement_end: location.measurement_end
                        ? dateTime(location.measurement_end).format("yyyy-LL-dd HH:mm")
                        : null,
                    directions: directionsArr,
                });
            });
            return responseArr;
        } catch (err) {
            throw new GeneralError("Database error", "PedestriansLocationsModel", err, 500);
        }
    };

    public GetOne = async (id: number): Promise<object | null> => {
        return null;
    };
}
