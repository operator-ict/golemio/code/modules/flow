import { FlowContainer } from "#og/ioc/Di";
import { CacheHeaderMiddleware } from "@golemio/core/dist/output-gateway/CacheHeaderMiddleware";
import { checkErrors, pagination, paginationLimitMiddleware } from "@golemio/core/dist/output-gateway/Validation";
import { ContainerToken } from "@golemio/core/dist/output-gateway/ioc";
import { BaseRouter } from "@golemio/core/dist/output-gateway/routes/BaseRouter";
import { NextFunction, Request, Response, Router } from "@golemio/core/dist/shared/express";
import { query } from "@golemio/core/dist/shared/express-validator";
import { PedestriansLocationsModel, PedestriansMeasurementsModel } from "#og/models";

export class PedestriansRouter extends BaseRouter {
    protected pedestriansLocationsModel: PedestriansLocationsModel = new PedestriansLocationsModel();
    protected pedestriansMeasurementsModel: PedestriansMeasurementsModel = new PedestriansMeasurementsModel();
    protected cacheHeaderMiddleware: CacheHeaderMiddleware;

    public constructor() {
        super();
        this.cacheHeaderMiddleware = FlowContainer.resolve<CacheHeaderMiddleware>(ContainerToken.CacheHeaderMiddleware);
        this.router.get(
            "/locations",
            [
                query("locationId").optional().not().isEmpty({ ignore_whitespace: true }).not().isArray(),
                query("locationName").optional().not().isEmpty({ ignore_whitespace: true }).not().isArray(),
            ],
            pagination,
            checkErrors,
            paginationLimitMiddleware("PedestriansRouter"),
            this.cacheHeaderMiddleware.getMiddleware(60 * 60, 300),
            this.GetPedestriansLocations
        );

        this.router.get(
            "/measurements",
            [
                query("locationId").optional().not().isEmpty({ ignore_whitespace: true }).not().isArray(),
                query("directionId").optional().not().isEmpty({ ignore_whitespace: true }).not().isArray(),
                query("from").optional().isISO8601().not().isArray(),
                query("to").optional().isISO8601().not().isArray(),
            ],
            pagination,
            checkErrors,
            paginationLimitMiddleware("PedestriansRouter"),
            this.cacheHeaderMiddleware.getMiddleware(60 * 60, 300),
            this.GetPedestriansMeasurements
        );
    }

    public GetPedestriansLocations = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const data = await this.pedestriansLocationsModel.GetAll({
                locationId: req.query.locationId as string,
                locationName: req.query.locationName as string,
                limit: req.query.limit ? +req.query.limit : undefined,
                offset: req.query.offset ? +req.query.offset : undefined,
            });

            if (!data?.length) return res.status(404).send();
            else res.status(200).send(data);
        } catch (err) {
            next(err);
        }
    };

    public GetPedestriansMeasurements = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const data = await this.pedestriansMeasurementsModel.GetAll({
                locationId: req.query.locationId as string,
                directionId: req.query.directionId as string,
                limit: req.query.limit ? +req.query.limit : undefined,
                offset: req.query.offset ? +req.query.offset : undefined,
                from: req.query.from as string,
                to: req.query.to as string,
            });

            if (!data?.length) return res.status(404).send();
            else res.status(200).send(data);
        } catch (err) {
            next(err);
        }
    };
}

const pedestriansRouter: Router = new PedestriansRouter().router;

export { pedestriansRouter };
